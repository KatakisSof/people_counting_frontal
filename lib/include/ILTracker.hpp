// Copyright (C) 2019 Irida LABS
//
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <set>
#include <unordered_map>
#include <utility>
#include <algorithm>
#include <iterator>
#include <Eigen>
#include <sys/time.h>
#include <shared_mutex>
#include <mutex>

#include <inference_engine.hpp>
#include <samples/slog.hpp>
#include <samples/ocv_common.hpp>
#include "ILDetector.hpp"
#include <details/ie_exception.hpp>
#include "readVisualConfig.hpp"



const static int INFTY_COST = 1e+5;

using namespace std;
using namespace cv;
using namespace InferenceEngine;


///
/// \brief The TrackerParams struct stores parameters of PedestrianTracker
/// We can use it to define the inital parameters of all the different classes easily
///

struct TrackerParams {
    bool DRAW_IMAGE; ///< Min confidence of detection.
    bool DRAW_ENTRANCE_LINES; ///< Min confidence of detection.
    bool DRAW_FOOTFALL_REGIONS; ///< Min confidence of detection.
    bool DRAW_DWELL_REGIONS; ///< Min confidence of detection.
    bool DRAW_BOUNDING_BOXES; ///< Min confidence of detection.
    bool DRAW_CENTROIDS; ///< Min confidence of detection.
    bool DRAW_TRACKS; ///< Min confidence of detection.
    bool DRAW_ENTRANCE_COUNTERS; ///< Min confidence of detection.
    bool DRAW_FOOTFALL_COUNTERS; ///< Min confidence of detection.
    bool DRAW_DWELL_COUNTERS; ///< Min confidence of detection.

    std::string ENTRANCE_LINES_COLOR; ///< Min confidence of detection.
    std::string FOOTFALL_REGIONS_COLOR; ///< Min confidence of detection.
    std::string DWELL_REGIONS_COLOR; ///< Min confidence of detection.
    std::string BOUNDING_BOXES_COLOR; ///< Min confidence of detection.
    std::string CENTROIDS_COLOR; ///< Min confidence of detection.
    std::string ENTRANCE_COUNTERS_COLOR; ///< Min confidence of detection.
    std::string FOOTFALL_COUNTERS_COLOR; ///< Min confidence of detection.
    std::string DWELL_COUNTERS_COLOR; ///< Min confidence of detection.

    bool AUDIT_MODE;
    int FRAMES_INIT;

    float max_iou_distance = 0.60; //For Crowded scenarios must be high

    int max_age = 25; //For Crowded scenario must be low

    //int n_init = FRAMES_INIT;

    float reid_thr= 3.5;

    int bugdet = 30; //For Crowded scenario must be high

    vector<vector<Point2i>> ENTRANCE_LINES;
    // The width for the construction of the entrance regions //
    const int ENTRANCE_WIDTH = 150;
    // The set of footfall regions (rectangles) in the image plane. The format is: (x, y, width, height) //
    vector<vector<Point2i>> FOOTFALL_REGIONS;
    // The set of dwell regions (rectangles) in the image plane. The format is: (x, y, width, height) //
    vector<vector<Point2i>> DWELL_REGIONS;

    const std::string filename_;

    std::vector<cv::Scalar> colors_;

    TrackerParams(const std::string file);
};




///
/// \brief Important structures
///

enum TrackState{
    TS_NONE = 0,
    Tentative,
    Confirmed,
    Deleted
};

struct NewAndDelete{
    std::map<int, int> news_;// id, pos
    std::vector<int> deletes_;
};



struct RR {
    std::vector<std::pair<int, int> > matches;
    IDS unmatched_detections;
    IDS unmatched_tracks;
};



///
/// \brief Template class Matrix. It is used during the Hungarian optimazation
///
///
template <class T>
class Matrix {
public:
    Matrix();
    Matrix(const size_t rows, const size_t columns);
    Matrix(const std::initializer_list<std::initializer_list<T>> init);
    Matrix(const Matrix<T> &other);
    Matrix<T> & operator= (const Matrix<T> &other);
    ~Matrix();
    // all operations modify the matrix in-place.
    void resize(const size_t rows, const size_t columns, const T default_value = 0);
    void clear();
    T& operator () (const size_t x, const size_t y);
    const T& operator () (const size_t x, const size_t y) const;
    const T mmin() const;
    const T mmax() const;
    inline size_t minsize() { return ((m_rows < m_columns) ? m_rows : m_columns); }
    inline size_t columns() const { return m_columns;}
    inline size_t rows() const { return m_rows;}

    friend std::ostream& operator<<(std::ostream& os, const Matrix &matrix)
    {
        for (size_t row = 0 ; row < matrix.rows() ; row++ )
        {
            for (size_t col = 0 ; col < matrix.columns() ; col++ )
            {
                os.width(8);
                os << matrix(row, col) << ",";
            }
            os << std::endl;
        }
        return os;
    }

private:
    T **m_matrix;
    size_t m_rows;
    size_t m_columns;
};



///
/// \brief Moving average class. It is used during tracking drawings
///
///

Eigen::VectorXf _nn_cosine_distance(const FEATURESS &x,
                                    const FEATURESS &y);


class NearestNeighborDistanceMetric{
private:
    static std::shared_ptr<NearestNeighborDistanceMetric> self_;
    float matching_threshold_ = 0;
    unsigned int budget_ = 0;
    std::map<int, std::vector<FEATURE> > samples_;
public:
    float matching_threshold();

    static std::shared_ptr<NearestNeighborDistanceMetric> Instance();

    void Init(float matching_threshold, int budget);

    void partial_fit(const FEATURESS &features,
                     const IDS &ids,
                     const IDS &active_ids);


    struct DDS{
    public:
        void Push(int pos, const Eigen::VectorXf &dd){
            std::scoped_lock lock(mutex_);
            dds_.push_back(
                    std::make_pair(pos, dd)
            );
        }
        void Get(std::vector<std::pair<int, Eigen::VectorXf> > &dds){
            dds = dds_;
        }
    private:
        std::vector<std::pair<int, Eigen::VectorXf> > dds_;
        std::shared_mutex  mutex_;
    };

    DYNAMICM distance(const FEATURESS &features, const IDS &ids);

};


///
/// \brief Hungarian operation class
///
///

class HungarianOper {
public:
    static Eigen::Matrix<float, -1, 2> Solve(const DYNAMICM &cost_matrix);
};



///
/// \brief Kalman filter class
///
///

class KF{

public:
    VAR _motion_mat_;
    UPM _update_mat_;
    double _std_weight_position_;
    double _std_weight_velocity_;

    bool Init();

    KF();

    VAR Diag(const MEAN &mean) const;

    NVAR NDiag(const NMEAN &mean) const;

public:
    std::pair<MEAN, VAR> initiate(const DSBOX &measurement) const;

    std::pair<MEAN, VAR> predict(const MEAN &mean, const VAR &covariance) const;

    std::pair<MEAN, VAR>  update(const MEAN &mean,  const VAR &covariance, const DSBOX &measurement) const;


    Eigen::Matrix<float, 1, -1> gating_distance(const MEAN &meani, const VAR &covariance,
                                                const DSBOXS &measurements, bool only_position=false) const;

    std::pair<NMEAN, NVAR> _project(const MEAN &mean, const VAR &covariance) const;


};



///
/// \brief KalmanTrackerN class use the kalman filter in each track
///
///

class KalmanTrackerN{
public:
    using kalmanP = std::shared_ptr<KF>;

    kalmanP kalmanFilter;
    VAR ori_motion_mat_;
    int time_since_update_ = 0;
    MEAN mean_;
    VAR covariance_;
    int track_id = 0;
    int show_id = 0;

    std::vector<FEATURE> features_;
    //std::vector<Attribs> attribs_;
    int oriPos_;
    PERSON person;

private:
    int hits_ = 0;
    int age_ = 0;
    TrackState state_ = TS_NONE;
    int _n_init_;
    int _max_age_;
    int is_new =1;

    size_t filterlength = 0;
    size_t count = 0;
    std::vector<Rect> tracklet;
    std::vector<Rect> tracklet_filtered;
    std::vector<Point3i> tracklet_paint;

public:
    KalmanTrackerN(const Detection &detection,
                   int tid,
                   int n_init,
                   int max_age,
                   const FEATURE &feature,
                   bool featureFull, int oriPos);


    DSBOX to_tlwh() const;

    DSBOX to_tlbr();

    DSBOX to_xcwh();

    void predict();

    void update(const Detection &detection);

    void mark_missed();

    bool check_id();

    bool is_tentative();

    bool is_confirmed() const ;

    bool is_deleted();

    std::vector<Point3i> filter_tracklet();

    void moving_average(int start, int end, int kernel);

    PERSON  is_in_dwell(vector<vector<Point2i>> DWELL_REGIONS);

    Point2i is_in_footfall(vector<Point2i> footfall_region);

    void passed_entrance(vector<vector<Point2i>> entrance_lines, Mat &counters);

};

///
/// \brief Irida Tracker class that handles the trackers
///
///

typedef std::shared_ptr<KalmanTrackerN> KalmanTracker;

static class TTracker *p;

class TTracker{

public:
    std::vector<KalmanTracker> kalmanTrackers_;
private:
    float max_iou_distance_ = 0;
    int max_age_ = 0;
    int n_init_ = 0;
    int _next_id_ = 0;
    int show_id =0;

public:
    TTracker(float max_iou_distance, int max_age, int n_init);

    NewAndDelete update(const std::vector<Detection> &detections);


private:

    RR _match(const std::vector<Detection> &detections);


    int _NewTrack(const Detection &detection);
};


static Eigen::VectorXf _centFunOut(const DSBOX &bbox, const DSBOXS &candidates);


DYNAMICM getCostMatrixByNND(const std::vector<KalmanTracker> &kalmanTrackers,
                            const std::vector<Detection> &dets,
                            IDS *track_indices,
                            IDS *detection_indices);




///
/// \brief Center distance matching operation class
///
///

class center_matching{
private:
    static Eigen::VectorXf _iouFun(const DSBOX &bbox, const DSBOXS &candidates);

public:
    static DYNAMICM getCostMatrixByCenter(const std::vector<KalmanTracker> &tracks,
                                          const std::vector<Detection> &detections,
                                          IDS *track_indicesi=NULL,
                                          IDS *detection_indicesi=NULL);
};


///
/// \brief Linear Assigment operation class
///
///

typedef DYNAMICM (*GetCostMarixFun)(const std::vector<KalmanTracker> &tracks,
                                    const std::vector<Detection> &detections,
                                    IDS *track_indices,
                                    IDS *detection_indices);

class linear_assignment{
public:
    static RR min_cost_matching(
            const GetCostMarixFun &getCostMarixFun, float max_distance,
            const std::vector<KalmanTracker> &tracks,
            const std::vector<Detection> &detections,
            IDS *track_indicesi=NULL,
            IDS *detection_indicesi=NULL);

    static RR matching_cascade(
            const GetCostMarixFun &getCostMarixFun, float max_distance,
            int cascade_depth,
            const std::vector<KalmanTracker> &tracks,
            const std::vector<Detection> &detections,
            IDS *track_indicesi = NULL,
            IDS *detection_indicesi = NULL);

    static DYNAMICM gate_cost_matrix(
            const KF &kalmanFilter,
            DYNAMICM &cost_matrix,
            const std::vector<KalmanTracker> &tracks,
            const std::vector<Detection> &detections,
            IDS track_indices,
            IDS detection_indices,
            int gated_cost=INFTY_COST,
            bool only_position=false);
};


///
/// \brief Online pedestrian tracker algorithm implementation.
///
/// This class is implementation of pedestrian tracking system. It uses two
/// different appearance measures to compute affinity between bounding boxes:
/// some descriptors. Each time the assignment
/// problem is solved. The assignment problem in our case is how to establish
/// correspondence between existing tracklets and recently detected objects.
/// First step is to compute an affinity matrix between tracklets and
/// detections.
/// Second step is to solve the assignment problem using Kuhn-Munkres
/// algorithm.
///

class PedestrianTracker {
public:
    using TTrackerP = std::shared_ptr<TTracker>;
    ///
    /// \brief Constructor that creates an instance of the pedestrian tracker with
    /// parameters.
    /// \param[in] params - the pedestrian tracker parameters.
    ///
    explicit PedestrianTracker(const TrackerParams params);
    virtual ~PedestrianTracker() {};


    void Process(const cv::Mat &frame, std::vector<Detection> &detections,
                    int frame_idx, vector<vector<int>> &footfall_id);

    /////////////////////// Reads the Important Regions of the System ////////////////////////
    void vector2point(vector<vector<vector<int>>> data_in, vector<vector<Point>> &data_out);

    void ReadREGIONS(vector<vector<vector<int>>> footfallREG_v, vector<vector<vector<int>>> dwellREG_v, vector<vector<vector<int>>> entranceLIN_v);

    /////////////////////// Drawing functions of the System ////////////////////////
    void DefineColor(string index, Mat &color);

    //void DrawPoints(Mat im, vector<PERSON> people, string set_color, bool enable);
    void DrawPoints(Mat im, vector<vector<Point3i>> coords, string set_color, bool enable);

    void DrawLines(Mat im, vector<vector<Point2i>> coords, string set_color, bool enable);

    void DrawTracks(Mat im, vector<vector<Point3i>> coords, bool enable);

    void DrawRectangles(Mat im, std::vector<Detection> detect, string set_color, bool enable);

    void DrawPolygons(Mat im, vector<vector<Point2i>> coords, string set_color, bool enable);

    void DrawDwellCounters(Mat im, vector<PERSON> counters, string set_color, bool enable, bool second_enable);

    void DrawFootfallCounters(Mat im, Mat counters, string set_color, bool enable, bool second_enable);

    void DrawEntranceCounters(Mat im, Mat counters, string set_color, bool enable);

    /////////////////////// Visualize tracking ////////////////////////
    Mat Visualize(int frame_index, Mat im );

private:
    // Tracker.
    TTrackerP tt_;

    int num;
    vector<vector<int>> footfall_id;
    vector<Point3i> people;
    Mat entrance_counters;
    Mat footfall_counters;
    vector<PERSON> dwell_counters;
    std::vector<vector<Point3i>> tracklet_paint_;

    //All the detection provided from the Detector.
    std::vector<Detection> detect_;
    // All the Parameter of the Tracker.
    TrackerParams params_;



};

