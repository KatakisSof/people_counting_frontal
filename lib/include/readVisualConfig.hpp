#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>


class VisualConfigClass{

private:
    std::map<std::string,std::string> data;

    bool verb=false;

public:

    bool  verbosity;
    
    VisualConfigClass(std::string filename);

   	int get_int(const char * var);

    bool get_bool(const char * var);

    std::string get_string(const char * var);

    std::map<std::string,std::string> LoadConfig(std::string filename);

};

