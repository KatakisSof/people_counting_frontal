#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <cstdio>

#include "json.hpp"

#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;
using json = nlohmann::json;

typedef map<string, vector<vector<vector<int>>>> polygon;


class ILsystem{
public:
	// variables
	bool Verbosity; 
	vector<vector<vector<int>>> pdata; // keep polygon data
	polygon data; //public variable holding data
	
	// functions
	ILsystem(string jsonfile,  vector<string> Keys, bool Verbose);
	
	void read_entries(string jsonfile, vector<string> Keys);
	
	vector<vector<vector<int>>> get_polygon(string query);
	
	void print_polygon(string query);
	
	// fuction for saving tracks of people
	void save_tracks(vector<Point3i> people, int frame);
	
};
