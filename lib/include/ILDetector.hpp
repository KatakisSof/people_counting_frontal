// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#pragma once

#include <map>
#include <string>
#include <vector>
#include <opencv2/core/core.hpp>
#include <deque>
#include <iostream>
#include <unordered_map>
#include <Eigen>
#include <mutex>
#include <queue>
#include <chrono>
#include <condition_variable>

#include <inference_engine.hpp>
#include <samples/slog.hpp>
#include <ext_list.hpp>
#include <samples/ocv_common.hpp>
//#include "infer_request_wrap.hpp"
//#include "inputs_filling.hpp"
#include <samples/common.hpp>
//#include <samples/slog.hpp>
#include <samples/csv_dumper.hpp>

using namespace InferenceEngine;
#define SSD_EMPTY_DETECTIONS_INDICATOR -1.0


/**
 * @brief Declaration of important variables of the network
 */
typedef Eigen::Matrix<int, 1, 7, Eigen::RowMajor> PERSON;
typedef Eigen::Matrix<float, 1, 4, Eigen::RowMajor> DSBOX;
typedef Eigen::Matrix<float, -1, 4, Eigen::RowMajor> DSBOXS;
typedef Eigen::Matrix<float, 1, 8, Eigen::RowMajor> Attribs;
typedef Eigen::Matrix<float, 1, 256, Eigen::RowMajor> FEATURE;
typedef Eigen::Matrix<float, -1, 256, Eigen::RowMajor> FEATURESS;
typedef std::vector<int> IDS;
typedef Eigen::Matrix<float, 1, 2, Eigen::RowMajor> PT2;
typedef Eigen::Matrix<float, -1, -1, Eigen::RowMajor> DYNAMICM;
typedef Eigen::Matrix<float, 1, 8, Eigen::RowMajor> MEAN;
typedef Eigen::Matrix<float, 8, 8, Eigen::RowMajor> VAR;
typedef Eigen::Matrix<float, 1, 4, Eigen::RowMajor> NMEAN;
typedef Eigen::Matrix<float, 4, 4, Eigen::RowMajor> NVAR;
typedef Eigen::Matrix<float, 4, 8, Eigen::RowMajor> UPM;


/**
 * @brief Important Declaration parameters of the Detection of the network
 */

static std::string toStr(float in) {
    char chr[20] = { 0 };
    sprintf(chr, "%d", (int)in);
    std::string re(chr);
    return re;
}



typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::nanoseconds ns;

typedef std::function<void(size_t id, const double latency)> QueueCallbackFunction;

/// @brief Wrapper class for InferenceEngine::InferRequest. Handles asynchronous callbacks and calculates execution time.
class InferReqWrap final {
public:
    using Ptr = std::shared_ptr<InferReqWrap>;

    ~InferReqWrap() = default;

    explicit InferReqWrap(InferenceEngine::ExecutableNetwork& net, size_t id, QueueCallbackFunction callbackQueue) :
        _request(net.CreateInferRequest()),
        _id(id),
        _callbackQueue(callbackQueue) {
        _request.SetCompletionCallback(
                [&]() {
                    _endTime = Time::now();
                    _callbackQueue(_id, getExecutionTimeInMilliseconds());
                });
    }

    void startAsync() {
        _startTime = Time::now();
        _request.StartAsync();
    }

    void infer() {
        _startTime = Time::now();
        _request.Infer();
        _endTime = Time::now();
        _callbackQueue(_id, getExecutionTimeInMilliseconds());
    }

    std::map<std::string, InferenceEngine::InferenceEngineProfileInfo> getPerformanceCounts() {
        return _request.GetPerformanceCounts();
    }

    InferenceEngine::Blob::Ptr getBlob(const std::string &name) {
        return _request.GetBlob(name);
    }

    double getExecutionTimeInMilliseconds() const {
        auto execTime = std::chrono::duration_cast<ns>(_endTime - _startTime);
        return static_cast<double>(execTime.count()) * 0.000001;
    }

private:
    InferenceEngine::InferRequest _request;
    Time::time_point _startTime;
    Time::time_point _endTime;
    size_t _id;
    QueueCallbackFunction _callbackQueue;
};

class InferRequestsQueue final {
public:
    InferRequestsQueue(InferenceEngine::ExecutableNetwork& net, size_t nireq) {
        for (size_t id = 0; id < nireq; id++) {
            requests.push_back(std::make_shared<InferReqWrap>(net, id, std::bind(&InferRequestsQueue::putIdleRequest, this,
                                                                                 std::placeholders::_1,
                                                                                 std::placeholders::_2)));
            _idleIds.push(id);
        }
        resetTimes();
    }
    ~InferRequestsQueue() = default;

    void resetTimes() {
        _startTime = Time::time_point::max();
        _endTime = Time::time_point::min();
        _latencies.clear();
    }

    double getDurationInMilliseconds() {
        return std::chrono::duration_cast<ns>(_endTime - _startTime).count() * 0.000001;
    }

    void putIdleRequest(size_t id,
                        const double latency) {
        std::unique_lock<std::mutex> lock(_mutex);
        _latencies.push_back(latency);
        _idleIds.push(id);
        _endTime = std::max(Time::now(), _endTime);
        _cv.notify_one();
    }

    InferReqWrap::Ptr getIdleRequest() {
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [this]{ return _idleIds.size() > 0; });
        auto request = requests.at(_idleIds.front());
        _idleIds.pop();
        _startTime = std::min(Time::now(), _startTime);
        return request;
    }

    void waitAll() {
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [this]{ return _idleIds.size() == requests.size(); });
    }

    std::vector<double> getLatencies() {
        return _latencies;
    }

    std::vector<InferReqWrap::Ptr> requests;

private:
    std::queue<size_t>_idleIds;
    std::mutex _mutex;
    std::condition_variable _cv;
    Time::time_point _startTime;
    Time::time_point _endTime;
    std::vector<double> _latencies;
};


/**
 * @brief Base class of the Detection of the network
 */

struct Detection {
    DSBOX tlwh_;
    float confidence_;
    FEATURE feature_;

    int oriPos_ = -1;

    Detection(const DSBOX &tlwh, float confidence, const FEATURE &feature);

    DSBOX to_tlbr() const;

    DSBOX to_xyah() const;

};



/**
 * @brief Base class of config for network
 */
struct CnnConfig {
    explicit CnnConfig(const std::string& path_to_model,
                       const std::string& path_to_weights)
        : path_to_model(path_to_model), path_to_weights(path_to_weights) {}

    /** @brief Path to model description */
    std::string path_to_model;
    /** @brief Path to model weights */
    std::string path_to_weights;
    /** @brief Maximal size of batch */
    int max_batch_size{1};
};



class REIDvector {


private:
    InferenceEngine::InferRequest::Ptr request;
    CnnConfig config_;
    InferenceEngine::InferencePlugin plugin_;

    InferenceEngine::ExecutableNetwork net_;
    std::string input_name_;
    std::string output_name_;
    int enqueued_frames_ = 0;
    bool results_fetched_ = false;
    int frame_idx_ = -1;
    bool is_async = true;

    std::vector<std::vector<float>> globalReIdVec;

    //InferRequestsQueue inferRequestsQueue(exeNetwork, nireq);

    //InferenceEngine::fillBlobs(inputFiles, batchSize, inputInfo, inferRequestsQueue.requests);

    void enqueue(const cv::Mat &frame);
    void submitRequest();
    void wait();
    void fetchResults();


public:

    explicit REIDvector(const std::string& path_to_model,
                        const std::string& path_to_weights,
                        const InferenceEngine::InferencePlugin& plugin);


    void submitFrame(const cv::Mat &frame, int frame_idx);
    void waitAndFetchResults();

    const std::vector<std::vector<float>>& getResults() const;
    //void PrintPerformanceCounts();
};





class REIDvector_second {


private:
    InferenceEngine::InferRequest::Ptr request;
    CnnConfig config_;
    InferenceEngine::InferencePlugin plugin_;

    InferenceEngine::ExecutableNetwork net_;
    std::string input_name_;
    std::string output_name_;
    int enqueued_frames_ = 0;
    bool results_fetched_ = false;
    int frame_idx_ = -1;
    bool is_async = true;

    std::vector<std::vector<float>> globalReIdVec;

    InferRequestsQueue inferRequestsQueue(exeNetwork, nireq);

    //InferRequestsQueue inferRequestsQueue(exeNetwork, nireq);

    //InferenceEngine::fillBlobs(inputFiles, batchSize, inputInfo, inferRequestsQueue.requests);

    void enqueue(const cv::Mat &frame);
    void submitRequest();
    void wait();
    void fetchResults();


public:

    explicit REIDvector_second(const std::string& path_to_model,
                               const std::string& path_to_weights,
                            const InferenceEngine::InferencePlugin& plugin);


    void submitFrame(const cv::Mat &frame, int frame_idx);
    void waitAndFetchResults();

    const std::vector<std::vector<float>>& getResults() const;
    //void PrintPerformanceCounts();
};




struct DetectorConfig : public CnnConfig {
    explicit DetectorConfig(const std::string& path_to_model,
                            const std::string& path_to_weights)
        : CnnConfig(path_to_model, path_to_weights) {}

    float confidence_threshold{0.5f};
    float increase_scale_x{1.f};
    float increase_scale_y{1.f};
    bool is_async = false;
    int input_h = 480;
    int input_w = 640;
};




class ObjectDetector {
private:
    using Descriptor = std::shared_ptr<REIDvector>;

    const std::string FLAGS_devices = "MYRIAD";
    std::vector<std::string> devices{FLAGS_devices};

    //const std::string &reid_model = "/home/sofoklis/Pictures/NewPeopleTrLib/models/dldt/person-reidentification-retail-0031-fp16.xml";
    const std::string reid_model = "/home/sofoklis/Pictures/NewPeopleTrLib/models/Retail/object_reidentification/pedestrian/rmnet_based/0079/dldt/person-reidentification-retail-0079-fp16.xml";
    const std::string reid_weights = fileNameNoExt(reid_model) + ".bin";


    const std::string det_model = "/home/sofoklis/Pictures/NewPeopleTrLib/models/Inference/IR/Embede_less.xml";
    //const std::string det_model = "/home/sofoklis/Pictures/NewPeopleTrLib//models/squeezeNet-SSD/squeezenet_ssd_fp16.xml";
    const std::string det_weights = fileNameNoExt(det_model) + ".bin";
    //std::map<std::string, uint32_t> device_nstreams = parseValuePerDevice(devices, 4);

    Descriptor vector_;

    InferenceEngine::InferRequest::Ptr request;
    DetectorConfig config_= DetectorConfig(det_model, det_weights);
    CnnConfig REIDconfig_ = CnnConfig(reid_model, reid_weights);



    InferenceEngine::ExecutableNetwork net_;
    std::string input_name_;
    std::string output_name_;
    long unsigned int max_detections_count_;
    int object_size_;
    int enqueued_frames_ = 0;
    float width_ = 0;
    float height_ = 0;
    bool results_fetched_ = false;
    int frame_idx_ = -1;
    cv::Mat frame_;
    std::vector<Detection> results_;


    void enqueue(const cv::Mat &frame);
    void submitRequest();
    void wait();
    void fetchResults();

public:
    explicit ObjectDetector();

    void submitFrame(const cv::Mat &frame, int frame_idx);
    void waitAndFetchResults();

    std::vector<Detection>& getResults();

    void printResults(cv::Mat img,  int frame_idx);

    //void PrintPerformanceCounts();

    //void PrintPerformanceCounts_REID();

    void SaveResults(int frame_idx);
};
