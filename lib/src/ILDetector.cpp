// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#include "ILDetector.hpp"
#include <algorithm>

#include <functional>
#include <inference_engine.hpp>


using namespace InferenceEngine;



///
/// \brief Function to load the plugin at the proper device
///
///
///

std::map<std::string, InferencePlugin>
LoadPluginForDevices(const std::vector<std::string>& devices,
                     const std::string& custom_cpu_library,
                     const std::string& custom_cldnn_kernels,
                     bool should_use_perf_counter) {
    std::map<std::string, InferencePlugin> plugins_for_devices;

    for (const auto &device : devices) {
        if (plugins_for_devices.find(device) != plugins_for_devices.end()) {
            continue;
        }
        std::cout << "Loading plugin " << device << std::endl;
        InferencePlugin plugin = PluginDispatcher().getPluginByDevice(device);
        //printPluginVersion(plugin, std::cout);
        /** Load extensions for the CPU plugin **/
        if ((device.find("CPU") != std::string::npos)) {
            plugin.AddExtension(std::make_shared<Extensions::Cpu::CpuExtensions>());
            if (!custom_cpu_library.empty()) {
                // CPU(MKLDNN) extensions are loaded as a shared library and passed as a pointer to base extension
                auto extension_ptr = make_so_pointer<IExtension>(custom_cpu_library);
                plugin.AddExtension(std::static_pointer_cast<IExtension>(extension_ptr));
            }
        } else if (!custom_cldnn_kernels.empty()) {
            // Load Extensions for other plugins not CPU
            plugin.SetConfig({{PluginConfigParams::KEY_CONFIG_FILE,
                                      custom_cldnn_kernels}});
        }
        if (device.find("CPU") != std::string::npos || device.find("GPU") != std::string::npos) {
            plugin.SetConfig({{PluginConfigParams::KEY_DYN_BATCH_ENABLED, PluginConfigParams::YES}});
        }


        if (should_use_perf_counter)
            plugin.SetConfig({{PluginConfigParams::KEY_PERF_COUNT, PluginConfigParams::YES}});
        plugins_for_devices[device] = plugin;
    }
    return plugins_for_devices;
}




void fillBlobImage(Blob::Ptr& inputBlob,
                  const std::vector<std::string>& filePaths,
                  const size_t& batchSize,
                  const InputInfo& info,
                  const size_t& requestId,
                  const size_t& inputId,
                  const size_t& inputSize) {
    auto inputBlobData = inputBlob->buffer().as<uint8_t*>();
    const TensorDesc& inputBlobDesc = inputBlob->getTensorDesc();

    /** Collect images data ptrs **/
    std::vector<std::shared_ptr<uint8_t>> vreader;
    vreader.reserve(batchSize);

    for (size_t i = 0ULL, inputIndex = requestId*batchSize*inputSize + inputId; i < batchSize; i++, inputIndex += inputSize) {
        inputIndex %= filePaths.size();

        slog::info << "Prepare image " << filePaths[inputIndex] << slog::endl;
        FormatReader::ReaderPtr reader(filePaths[inputIndex].c_str());
        if (reader.get() == nullptr) {
            slog::warn << "Image " << filePaths[inputIndex] << " cannot be read!" << slog::endl << slog::endl;
            continue;
        }

        /** Getting image data **/
        TensorDesc desc = info.getTensorDesc();
        std::shared_ptr<uint8_t> imageData(reader->getData(getTensorWidth(desc), getTensorHeight(desc)));
        if (imageData) {
            vreader.push_back(imageData);
        }
    }

    /** Fill input tensor with images. First b channel, then g and r channels **/
    const size_t numChannels = getTensorChannels(inputBlobDesc);
    const size_t imageSize = getTensorWidth(inputBlobDesc) * getTensorHeight(inputBlobDesc);
    /** Iterate over all input images **/
    for (size_t imageId = 0; imageId < vreader.size(); ++imageId) {
        /** Iterate over all pixel in image (b,g,r) **/
        for (size_t pid = 0; pid < imageSize; pid++) {
            /** Iterate over all channels **/
            for (size_t ch = 0; ch < numChannels; ++ch) {
                /**          [images stride + channels stride + pixel id ] all in bytes            **/
                inputBlobData[imageId * imageSize * numChannels + ch * imageSize + pid] = vreader.at(imageId).get()[pid*numChannels + ch];
            }
        }
    }
}


namespace {
cv::Rect TruncateToValidRect(const cv::Rect& rect,
                             const cv::Size& size) {
    auto tl = rect.tl(), br = rect.br();
    tl.x = std::max(0, std::min(size.width - 1, tl.x));
    tl.y = std::max(0, std::min(size.height - 1, tl.y));
    br.x = std::max(0, std::min(size.width, br.x));
    br.y = std::max(0, std::min(size.height, br.y));
    int w = std::max(0, br.x - tl.x);
    int h = std::max(0, br.y - tl.y);


    return cv::Rect(tl.x, tl.y, w, h);
}

cv::Rect IncreaseRect(const cv::Rect& r, float coeff_x,
                      float coeff_y)  {
    cv::Point2f tl = r.tl();
    cv::Point2f br = r.br();
    cv::Point2f c = (tl * 0.5f) + (br * 0.5f);
    cv::Point2f diff = c - tl;
    cv::Point2f new_diff{diff.x * coeff_x, diff.y * coeff_y};
    cv::Point2f new_tl = c - new_diff;
    cv::Point2f new_br = c + new_diff;

    cv::Point new_tl_int {static_cast<int>(std::floor(new_tl.x)), static_cast<int>(std::floor(new_tl.y))};
    cv::Point new_br_int {static_cast<int>(std::ceil(new_br.x)), static_cast<int>(std::ceil(new_br.y))};

    return cv::Rect(new_tl_int, new_br_int);
}
}  // namespace


inline bool IsInRange(float val, float min, float max) {
    return min <= val && val <= max;
}

inline bool IsInRange(float val, cv::Vec2f range) {
    return IsInRange(val, range[0], range[1]);
}



//-------------------------------------------------------------------//
// REID
//-------------------------------------------------------------------//

void REIDvector::submitRequest() {
    if (request == nullptr) return;
    if (!enqueued_frames_) return;
    enqueued_frames_ = 0;
    results_fetched_ = false;
    globalReIdVec.clear();

    if (is_async) {
        request->StartAsync();
    } else {
        request->Infer();
    }
}

const std::vector<std::vector<float>>& REIDvector::getResults() const {
    return globalReIdVec;
}


void REIDvector::enqueue(const cv::Mat &frame) {
    if (!request) {
        request = net_.CreateInferRequestPtr();
    }

    //width_ = static_cast<float>(frame.cols);
    //height_ = static_cast<float>(frame.rows);

    Blob::Ptr inputBlob = request->GetBlob(input_name_);

    matU8ToBlob<uint8_t>(frame, inputBlob);

    enqueued_frames_ = 1;
}

void REIDvector::submitFrame(const cv::Mat &frame, int frame_idx) {
    frame_idx_ = frame_idx;
    enqueue(frame);
    submitRequest();
}

REIDvector::REIDvector(
        const std::string& path_to_model,
        const std::string& path_to_weights,
        const InferenceEngine::InferencePlugin& plugin) :
        config_(path_to_model, path_to_weights),
        plugin_(plugin) {

    CNNNetReader net_reader;

    net_reader.ReadNetwork(config_.path_to_model);
    net_reader.ReadWeights(config_.path_to_weights);
    if (!net_reader.isParseSuccess()) {
        THROW_IE_EXCEPTION << "Cannot load model";
    }

    InputsDataMap inputInfo(net_reader.getNetwork().getInputsInfo());
    if (inputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "REID network should have only one input";
    }
    InputInfo::Ptr inputInfoFirst = inputInfo.begin()->second;
    inputInfoFirst->setPrecision(Precision::U8);
    inputInfoFirst->getInputData()->setLayout(Layout::NCHW);

    //SizeVector input_dims = inputInfoFirst->getInputData()->getTensorDesc().getDims();
    /*input_dims[2] = config_.input_h;
    input_dims[3] = config_.input_w;*/

    /*std::map<std::string, SizeVector> input_shapes;
    input_shapes[inputInfo.begin()->first] = input_dims;
    net_reader.getNetwork().reshape(input_shapes);*/

    OutputsDataMap outputInfo(net_reader.getNetwork().getOutputsInfo());
    if (outputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "REID network should have only one output";
    }
    DataPtr& _output = outputInfo.begin()->second;
    output_name_ = outputInfo.begin()->first;

    const CNNLayerPtr outputLayer = net_reader.getNetwork().getLayerByName(output_name_.c_str());
    /*if (outputLayer->type != "DetectionOutput") {
        THROW_IE_EXCEPTION << "Face Detection network output layer(" + outputLayer->name +
            ") should be DetectionOutput, but was " +  outputLayer->type;
    }
    if (outputLayer->params.find("num_classes") == outputLayer->params.end()) {
        THROW_IE_EXCEPTION << "Face Detection network output layer (" +
            output_name_ + ") should have num_classes integer attribute";
    }*/

    /*const SizeVector outputDims = _output->getTensorDesc().getDims();
    max_detections_count_ = outputDims[2];
    object_size_ = outputDims[3];
    if (object_size_ != 7) {
        THROW_IE_EXCEPTION << "Face Detection network output layer should have 7 as a last dimension";
    }
    if (outputDims.size() != 4) {
        THROW_IE_EXCEPTION << "Face Detection network output dimensions not compatible shoulld be 4, but was " +
            std::to_string(outputDims.size());
    }*/
    _output->setPrecision(Precision::FP32);
    _output->setLayout(TensorDesc::getLayoutByDims(_output->getDims()));

    input_name_ = inputInfo.begin()->first;
    net_ = plugin_.LoadNetwork(net_reader.getNetwork(), {});
}

void REIDvector::wait() {
    if (!request || !is_async) return;
    request->Wait(InferenceEngine::IInferRequest::WaitMode::RESULT_READY);
}

void REIDvector::fetchResults() {

    globalReIdVec.clear();
    if (results_fetched_) return;
    results_fetched_ = true;
    auto *data = request->GetBlob(output_name_)->buffer().as<float *>();
    globalReIdVec.emplace_back(std::vector<float>(data, data + 256)); //64
}

void REIDvector::waitAndFetchResults() {
    wait();
    fetchResults();
}

/*void REIDvector::PrintPerformanceCounts() {
    std::cout << "\n\nPerformance counts for object REID" << std::endl << std::endl;
    ::printPerformanceCounts(request->GetPerformanceCounts(), std::cout, false);
}
*/

//-------------------------------------------------------------------//
// Detection
//-------------------------------------------------------------------//

/*Detection::Detection(const DSBOX &tlwh, float confidence, const FEATURE &feature,
                     const Attribs &PersonAtt) {
    tlwh_ 		= tlwh;
    confidence_ = confidence;
    feature_    = feature;
    attribs_    = PersonAtt;


}*/




//-------------------------------------------------------------------//
// REID Second
//-------------------------------------------------------------------//

void REIDvector_second::submitRequest() {
    if (request == nullptr) return;
    if (!enqueued_frames_) return;
    enqueued_frames_ = 0;
    results_fetched_ = false;
    globalReIdVec.clear();

    if (is_async) {
        request->StartAsync();
    } else {
        request->Infer();
    }
}

const std::vector<std::vector<float>>& REIDvector::getResults() const {
    return globalReIdVec;
}


void REIDvector_second::enqueue(const cv::Mat &frame) {
    if (!request) {
        request = net_.CreateInferRequestPtr();
    }

    //width_ = static_cast<float>(frame.cols);
    //height_ = static_cast<float>(frame.rows);

    Blob::Ptr inputBlob = request->GetBlob(input_name_);

    matU8ToBlob<uint8_t>(frame, inputBlob);

    enqueued_frames_ = 1;
}

void REIDvector_second::submitFrame(const cv::Mat &frame, int frame_idx) {
    frame_idx_ = frame_idx;
    enqueue(frame);
    submitRequest();
}

REIDvector_second::REIDvector_second(
        const std::string& path_to_model,
        const std::string& path_to_weights,
        const InferenceEngine::InferencePlugin& plugin) :
        config_(path_to_model, path_to_weights),
        plugin_(plugin) {

    CNNNetReader net_reader;

    net_reader.ReadNetwork(config_.path_to_model);
    net_reader.ReadWeights(config_.path_to_weights);
    if (!net_reader.isParseSuccess()) {
        THROW_IE_EXCEPTION << "Cannot load model";
    }

    InputsDataMap inputInfo(net_reader.getNetwork().getInputsInfo());
    if (inputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "REID network should have only one input";
    }
    InputInfo::Ptr inputInfoFirst = inputInfo.begin()->second;
    inputInfoFirst->setPrecision(Precision::U8);
    inputInfoFirst->getInputData()->setLayout(Layout::NCHW);

    //SizeVector input_dims = inputInfoFirst->getInputData()->getTensorDesc().getDims();
    /*input_dims[2] = config_.input_h;
    input_dims[3] = config_.input_w;*/

    /*std::map<std::string, SizeVector> input_shapes;
    input_shapes[inputInfo.begin()->first] = input_dims;
    net_reader.getNetwork().reshape(input_shapes);*/

    OutputsDataMap outputInfo(net_reader.getNetwork().getOutputsInfo());
    if (outputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "REID network should have only one output";
    }
    DataPtr& _output = outputInfo.begin()->second;
    output_name_ = outputInfo.begin()->first;

    const CNNLayerPtr outputLayer = net_reader.getNetwork().getLayerByName(output_name_.c_str());
    /*if (outputLayer->type != "DetectionOutput") {
        THROW_IE_EXCEPTION << "Face Detection network output layer(" + outputLayer->name +
            ") should be DetectionOutput, but was " +  outputLayer->type;
    }
    if (outputLayer->params.find("num_classes") == outputLayer->params.end()) {
        THROW_IE_EXCEPTION << "Face Detection network output layer (" +
            output_name_ + ") should have num_classes integer attribute";
    }*/

    /*const SizeVector outputDims = _output->getTensorDesc().getDims();
    max_detections_count_ = outputDims[2];
    object_size_ = outputDims[3];
    if (object_size_ != 7) {
        THROW_IE_EXCEPTION << "Face Detection network output layer should have 7 as a last dimension";
    }
    if (outputDims.size() != 4) {
        THROW_IE_EXCEPTION << "Face Detection network output dimensions not compatible shoulld be 4, but was " +
            std::to_string(outputDims.size());
    }*/
    _output->setPrecision(Precision::FP32);
    _output->setLayout(TensorDesc::getLayoutByDims(_output->getDims()));

    input_name_ = inputInfo.begin()->first;
    net_ = plugin_.LoadNetwork(net_reader.getNetwork(), {});
}

void REIDvector_second::wait() {
    if (!request || !is_async) return;
    request->Wait(InferenceEngine::IInferRequest::WaitMode::RESULT_READY);
}

void REIDvector_second::fetchResults() {

    globalReIdVec.clear();
    if (results_fetched_) return;
    results_fetched_ = true;
    auto *data = request->GetBlob(output_name_)->buffer().as<float *>();
    globalReIdVec.emplace_back(std::vector<float>(data, data + 256)); //64
}

void REIDvector_second::waitAndFetchResults() {
    wait();
    fetchResults();
}


//-------------------------------------------------------------------//
// REID Second
//-------------------------------------------------------------------//





Detection::Detection(const DSBOX &tlwh, float confidence, const FEATURE &feature) {
		tlwh_ 		= tlwh;
		confidence_ = confidence;
		feature_    = feature;
	}

DSBOX Detection::to_tlbr() const{
    DSBOX ret = tlwh_;
    ret(0, 2) += ret(0, 0);
    ret(0, 3) += ret(0, 1);
    return ret;
}

DSBOX Detection::to_xyah() const{
    DSBOX ret = tlwh_;
    ret(0, 0) += ret(0, 2) / 2;
    ret(0, 1) += ret(0, 3) / 2;
    ret(0, 2) /= ret(0, 3);
    return ret;
}


//-------------------------------------------------------------------//
// Object Detector
//-------------------------------------------------------------------//

void ObjectDetector::submitRequest() {
    if (request == nullptr) return;
    if (!enqueued_frames_) return;
    enqueued_frames_ = 0;
    results_fetched_ = false;
    results_.clear();

    if (config_.is_async) {
        request->StartAsync();
    } else {
        request->Infer();
    }
}

/*
const TrackedObjects& ObjectDetector::getResults() const {
    return results_;
}
*/

std::vector<Detection>& ObjectDetector::getResults() {
    return results_;
}

void ObjectDetector::enqueue(const cv::Mat &frame) {
    if (!request) {
        request = net_.CreateInferRequestPtr();
    }

    width_ = static_cast<float>(frame.cols);
    height_ = static_cast<float>(frame.rows);

    Blob::Ptr inputBlob = request->GetBlob(input_name_);

    matU8ToBlob<uint8_t>(frame, inputBlob);

    enqueued_frames_ = 1;
}

void ObjectDetector::submitFrame(const cv::Mat &frame, int frame_idx) {
    frame_ = frame;
    frame_idx_ = frame_idx;
    enqueue(frame);
    submitRequest();
}

/*ObjectDetector::ObjectDetector(
    const DetectorConfig& config,
    const CnnConfig& REIDconfig,
    const InferenceEngine::InferencePlugin& plugin) :
    config_(config),
    REIDconfig_(REIDconfig),
    plugin_(plugin) {*/
ObjectDetector::ObjectDetector() {

    std::map<std::string, InferencePlugin> plugins_for_devices =
            LoadPluginForDevices(devices, "", "", false);



    InferenceEngine::InferencePlugin plugin_ = plugins_for_devices[FLAGS_devices];

    vector_ = Descriptor(new REIDvector(REIDconfig_.path_to_model, REIDconfig_.path_to_weights, plugin_));

    CNNNetReader net_reader;

    //std::map<std::string, std::string> networkConfig;
    //networkConfig[PluginConfigParams::KEY_CPU_THROUGHPUT_STREAMS] = std::to_string(4);

    net_reader.ReadNetwork(config_.path_to_model);
    net_reader.ReadWeights(config_.path_to_weights);
    if (!net_reader.isParseSuccess()) {
        THROW_IE_EXCEPTION << "Cannot load model";
    }

    InputsDataMap inputInfo(net_reader.getNetwork().getInputsInfo());
    if (inputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "Face Detection network should have only one input";
    }
    InputInfo::Ptr inputInfoFirst = inputInfo.begin()->second;
    inputInfoFirst->setPrecision(Precision::U8);
    inputInfoFirst->getInputData()->setLayout(Layout::NCHW);

    SizeVector input_dims = inputInfoFirst->getInputData()->getTensorDesc().getDims();
    //input_dims[2] = config_.input_h;
    //input_dims[3] = config_.input_w;
    std::map<std::string, SizeVector> input_shapes;

    input_shapes[inputInfo.begin()->first] = input_dims;

    net_reader.getNetwork().reshape(input_shapes);

    OutputsDataMap outputInfo(net_reader.getNetwork().getOutputsInfo());
    if (outputInfo.size() != 1) {
        THROW_IE_EXCEPTION << "Face Detection network should have only one output";
    }
    DataPtr& _output = outputInfo.begin()->second;
    output_name_ = outputInfo.begin()->first;

    const CNNLayerPtr outputLayer = net_reader.getNetwork().getLayerByName(output_name_.c_str());
    if (outputLayer->type != "DetectionOutput") {
        THROW_IE_EXCEPTION << "Face Detection network output layer(" + outputLayer->name +
            ") should be DetectionOutput, but was " +  outputLayer->type;
    }

    if (outputLayer->params.find("num_classes") == outputLayer->params.end()) {
        THROW_IE_EXCEPTION << "Face Detection network output layer (" +
            output_name_ + ") should have num_classes integer attribute";
    }

    const SizeVector outputDims = _output->getTensorDesc().getDims();
    max_detections_count_ = outputDims[2];
    object_size_ = outputDims[3];
    if (object_size_ != 7) {
        THROW_IE_EXCEPTION << "Face Detection network output layer should have 7 as a last dimension";
    }
    if (outputDims.size() != 4) {
        THROW_IE_EXCEPTION << "Face Detection network output dimensions not compatible shoulld be 4, but was " +
            std::to_string(outputDims.size());
    }
    _output->setPrecision(Precision::FP32);
    _output->setLayout(TensorDesc::getLayoutByDims(_output->getDims()));

    input_name_ = inputInfo.begin()->first;


    net_ = plugin_.LoadNetwork(net_reader.getNetwork(), {});

    uint32_t nireq = 4;
    InferRequestsQueue inferRequestsQueue(net_, nireq);
}

void ObjectDetector::wait() {
    if (!request || !config_.is_async) return;
    request->Wait(InferenceEngine::IInferRequest::WaitMode::RESULT_READY);
}



void ObjectDetector::fetchResults() {
    results_.clear();
    if (results_fetched_) return;
    results_fetched_ = true;
    const float *data = request->GetBlob(output_name_)->buffer().as<float *>();

    //multithreading here can be crucial (NOT working)

    //parallel_for( tbb::blocked_range<int>(0, max_detections_count_),
    //              [&](tbb::blocked_range<int> r){
    //for (size_t det_id = r.begin(); det_id < r.end(); ++det_id){
     for (int det_id = 0; det_id < max_detections_count_; ++det_id) {
            const int start_pos = det_id * object_size_;

            const float batchID = data[start_pos];
            if (batchID == SSD_EMPTY_DETECTIONS_INDICATOR) {
                break;
            }

            const float score = std::min(std::max(0.0f, data[start_pos + 2]), 1.0f);
            const float x0 =
                std::min(std::max(0.0f, data[start_pos + 3]), 1.0f) * width_;
            const float y0 =
                std::min(std::max(0.0f, data[start_pos + 4]), 1.0f) * height_;
            const float x1 =
                std::min(std::max(0.0f, data[start_pos + 5]), 1.0f) * width_;
            const float y1 =
                std::min(std::max(0.0f, data[start_pos + 6]), 1.0f) * height_;

            /*TrackedObject object;
            object.confidence = score;
            object.rect = cv::Rect(cv::Point(static_cast<int>(round(static_cast<double>(x0))),
                                             static_cast<int>(round(static_cast<double>(y0)))),
                                   cv::Point(static_cast<int>(round(static_cast<double>(x1))),
                                             static_cast<int>(round(static_cast<double>(y1)))));

            object.rect = TruncateToValidRect(IncreaseRect(object.rect,
                                                           config_.increase_scale_x,
                                                           config_.increase_scale_y),
                                              cv::Size(static_cast<int>(width_), static_cast<int>(height_)));
            object.frame_idx = frame_idx_;

            if (object.confidence > config_.confidence_threshold && object.rect.area() > 0) {
                results_.emplace_back(object);
            }*/

            cv::Rect rect;
            rect = cv::Rect(cv::Point(static_cast<int>(round(static_cast<double>(x0))),
                                             static_cast<int>(round(static_cast<double>(y0)))),
                                   cv::Point(static_cast<int>(round(static_cast<double>(x1))),
                                             static_cast<int>(round(static_cast<double>(y1)))));

            rect = TruncateToValidRect(IncreaseRect(rect,
                                                    config_.increase_scale_x,
                                                    config_.increase_scale_y),
                                              cv::Size(static_cast<int>(width_), static_cast<int>(height_)));

            if (score > config_.confidence_threshold && rect.area() > 0) {

                bool flag = true;

                float aspect_ratio = static_cast<float>(rect.height) / static_cast<float>(rect.width);
                cv::Vec2f bbox_aspect_ratios_range(1.266f, 4.1f);
                cv::Vec2f bbox_heights_range(20.f, 360.f);
                cv::Vec2f bbox_widths_range(30.f, 180.f);

                if ( IsInRange(aspect_ratio, bbox_aspect_ratios_range) &&
                     IsInRange(static_cast<float>(rect.height), bbox_heights_range) &&
                     IsInRange(static_cast<float>(rect.width), bbox_widths_range)) {

                    vector_->submitFrame(frame_(rect).clone(), frame_idx_);
                    vector_->waitAndFetchResults();
                    auto REIDvector = vector_->getResults();

                    FEATURE ft;

                    for (int i = 0; i < 256; i++) {
                        ft(0, i) = REIDvector.at(0)[i];
                    }

                    DSBOX box;
                    box(0) = rect.x;
                    box(1) = rect.y;
                    box(2) = rect.width;
                    box(3) = rect.height;

                    Detection det(box, score, ft);
                    results_.push_back(det);
                  }
            }
     }
}


void ObjectDetector::waitAndFetchResults() {
    wait();
    fetchResults();
}

void ObjectDetector::printResults(cv::Mat frame, int frame_idx){
    //draw boxes and render frame
    for (const auto &detection : results_) {
        DSBOX box = detection.tlwh_;

        cv::Rect rc;
        rc.x = box(0);
        rc.y = box(1);
        rc.width = box(2);
        rc.height = box(3);

        cv::rectangle(frame, rc, cv::Scalar(255, 0, 0), 3);
    }

    std::string disp = toStr(frame_idx);
    cv::putText(frame,
                disp,
                cv::Point(10, 10),
                cv::FONT_HERSHEY_SIMPLEX,
                0.6,
                cv::Scalar(0, 0, 255));
    //cv::Mat mm(frame);
    //resize(mm, frame, cv::Size(), 0.5, 0.5);
    imshow("Detector", frame);
    cv::waitKey(1);
}

void ObjectDetector::SaveResults( int frame_idx){
    //draw boxes and render frame
    std::ofstream outfile;

    outfile.open("/home/sofoklis/Pictures/NewPeopleTrLib/DeepSort/dets4.txt", std::ios_base::app);//std::ios_base::app


    for (const auto &detection : results_) {
        DSBOX box   = detection.tlwh_;
        float conf_ = detection.confidence_;
        outfile << toStr(frame_idx) <<"," <<  toStr(box(0)) <<"," <<  toStr(box(1)) << "," <<  toStr(box(2)) << "," <<  toStr(box(3))<< "," << toStr(conf_)<< '\n';
        /*cv::Rect rc;
        rc.x = box(0);
        rc.y = box(1);
        rc.width = box(2);
        rc.height = box(3);
        */

    }

}

/*void ObjectDetector::PrintPerformanceCounts() {
    std::cout << "\n\nPerformance counts for object detector" << std::endl << std::endl;
    ::printPerformanceCounts(request->GetPerformanceCounts(), std::cout, false);
}

void ObjectDetector::PrintPerformanceCounts_REID() {
    vector_->PrintPerformanceCounts();
}
*/