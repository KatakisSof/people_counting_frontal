#include "ILTracker.hpp"
#include <ie_plugin_config.hpp>
#include <ext_list.hpp>
#include <stdio.h>
#include <algorithm>    // std::min

std::shared_ptr<NearestNeighborDistanceMetric> NearestNeighborDistanceMetric::self_;

//Assertions for Debugging
#define PT_CHECK(cond) IE_ASSERT(cond) << " "
#define XYZMIN(x, y) (x)<(y)?(x):(y)
#define XYZMAX(x, y) (x)>(y)?(x):(y)
#define PT_CHECK_BINARY(actual, expected, op) \
    IE_ASSERT(actual op expected) << ". " \
        << actual << " vs " << expected << ".  "
#define PT_CHECK_EQ(actual, expected) PT_CHECK_BINARY(actual, expected, ==)
#define PT_CHECK_NE(actual, expected) PT_CHECK_BINARY(actual, expected, !=)
#define PT_CHECK_LT(actual, expected) PT_CHECK_BINARY(actual, expected, <)
#define PT_CHECK_GT(actual, expected) PT_CHECK_BINARY(actual, expected, >)
#define PT_CHECK_LE(actual, expected) PT_CHECK_BINARY(actual, expected, <=)
#define PT_CHECK_GE(actual, expected) PT_CHECK_BINARY(actual, expected, >=)


///
/// \brief Generation of the different track colors
///
///
///

//-------------------------------------------------------------------//


std::vector<cv::Scalar> GenRandomColors(int colors_num) {
    std::vector<cv::Scalar> colors(colors_num);
    for (int i = 0; i < colors_num; i++) {
        colors[i] = cv::Scalar(static_cast<uchar>(255. * rand() / RAND_MAX),  // NOLINT
                               static_cast<uchar>(255. * rand() / RAND_MAX),  // NOLINT
                               static_cast<uchar>(255. * rand() / RAND_MAX));  // NOLINT
    }
    return colors;
}



//-------------------------------------------------------------------//


///
/// \brief Reading of all the parameters from a string file
///
///
///

TrackerParams::TrackerParams(const std::string file)
        : filename_(file) {

        VisualConfigClass VisualConfig(filename_);

        DRAW_IMAGE = VisualConfig.get_bool("DRAW_IMAGE");
        DRAW_ENTRANCE_LINES = VisualConfig.get_bool("DRAW_ENTRANCE_LINES");
        DRAW_FOOTFALL_REGIONS = VisualConfig.get_bool("DRAW_FOOTFALL_REGIONS");
        DRAW_DWELL_REGIONS = VisualConfig.get_bool("DRAW_DWELL_REGIONS");
        DRAW_BOUNDING_BOXES = VisualConfig.get_bool("DRAW_BOUNDING_BOXES");
        DRAW_CENTROIDS = VisualConfig.get_bool("DRAW_CENTROIDS");
        DRAW_TRACKS = VisualConfig.get_bool("DRAW_TRACKS");
        DRAW_ENTRANCE_COUNTERS = VisualConfig.get_bool("DRAW_ENTRANCE_COUNTERS");
        DRAW_FOOTFALL_COUNTERS = VisualConfig.get_bool("DRAW_FOOTFALL_COUNTERS");
        DRAW_DWELL_COUNTERS = VisualConfig.get_bool("DRAW_DWELL_COUNTERS");

        ENTRANCE_LINES_COLOR = VisualConfig.get_string("ENTRANCE_LINES_COLOR");
        FOOTFALL_REGIONS_COLOR = VisualConfig.get_string("FOOTFALL_REGIONS_COLOR");
        DWELL_REGIONS_COLOR = VisualConfig.get_string("DWELL_REGIONS_COLOR");
        BOUNDING_BOXES_COLOR = VisualConfig.get_string("BOUNDING_BOXES_COLOR");
        CENTROIDS_COLOR = VisualConfig.get_string("CENTROIDS_COLOR");
        ENTRANCE_COUNTERS_COLOR = VisualConfig.get_string("ENTRANCE_COUNTERS_COLOR");
        FOOTFALL_COUNTERS_COLOR = VisualConfig.get_string("FOOTFALL_COUNTERS_COLOR");
        DWELL_COUNTERS_COLOR = VisualConfig.get_string("DWELL_COUNTERS_COLOR");

        AUDIT_MODE = VisualConfig.get_bool("AUDIT_MODE");
        FRAMES_INIT = VisualConfig.get_int("FRAMES_INIT");

        if (colors_.empty()) {
             int num_colors = 1000;
            colors_ = GenRandomColors(num_colors);
        }
}


///
/// \brief Validation that the parameter have the correct values
///
///
///
/// \param p
void ValidateParams(const TrackerParams &p) {

    PT_CHECK_GE(p.DRAW_IMAGE, 0);
    PT_CHECK_LE(p.DRAW_IMAGE, 1);

    PT_CHECK_GE(p.DRAW_ENTRANCE_LINES, 0);
    PT_CHECK_LE(p.DRAW_ENTRANCE_LINES, 1);

    PT_CHECK_GE(p.DRAW_FOOTFALL_REGIONS, 0);
    PT_CHECK_LE(p.DRAW_FOOTFALL_REGIONS, 1);

    PT_CHECK_GE(p.DRAW_DWELL_REGIONS, 0);
    PT_CHECK_LE(p.DRAW_DWELL_REGIONS, 1);

    PT_CHECK_GE(p.DRAW_BOUNDING_BOXES, 0);
    PT_CHECK_LE(p.DRAW_BOUNDING_BOXES, 1);

    PT_CHECK_GE(p.DRAW_CENTROIDS, 0);
    PT_CHECK_LE(p.DRAW_CENTROIDS, 1);

    PT_CHECK_GE(p.DRAW_TRACKS, 0);
    PT_CHECK_LE(p.DRAW_TRACKS, 1);

    PT_CHECK_GE(p.DRAW_ENTRANCE_COUNTERS, 0);
    PT_CHECK_LE(p.DRAW_ENTRANCE_COUNTERS, 1);

    PT_CHECK_GE(p.DRAW_FOOTFALL_COUNTERS, 0);
    PT_CHECK_LE(p.DRAW_FOOTFALL_COUNTERS, 1);

    PT_CHECK_GE(p.DRAW_DWELL_COUNTERS, 0);
    PT_CHECK_LE(p.DRAW_DWELL_COUNTERS, 1);

    PT_CHECK_GE(p.AUDIT_MODE, 0);
    PT_CHECK_LE(p.AUDIT_MODE, 1);

    PT_CHECK_GE(p.FRAMES_INIT, 1);
    PT_CHECK_LE(p.FRAMES_INIT, 10);

    PT_CHECK_GE(p.max_iou_distance, 0.0f);
    PT_CHECK_LE(p.max_iou_distance, 1.0f);

    PT_CHECK_GE(p.max_age, 1);
    PT_CHECK_LE(p.max_age, 100);

    PT_CHECK_GE(p.reid_thr, 0.0f);
    PT_CHECK_LE(p.reid_thr, 12.0f);

    PT_CHECK_GE(p.bugdet, 1);
    PT_CHECK_LE(p.bugdet, 100);
}


///
/// \brief Important structs during the optimization
///
///
///
const double chi2inv95[10] = {
        0,
        3.8415,
        5.9915,
        7.8147,
        9.4877,
        11.070,
        12.592,
        14.067,
        15.507,
        16.919 };



template <typename T>
std::ostream &operator<<(std::ostream &os, const std::deque<T> &v) {
    os << "[\n";
    if (!v.empty()) {
        auto itr = v.begin();
        os << *itr;
        for (++itr; itr != v.end(); ++itr) os << ",\n" << *itr;
    }
    os << "\n]";
    return os;
}

///
/// \brief Stream output operator for vector of elements.
/// \param[in,out] os Output stream.
/// \param[in] v Vector of elements.
///
template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
    os << "[\n";
    if (!v.empty()) {
        auto itr = v.begin();
        os << *itr;
        for (++itr; itr != v.end(); ++itr) os << ",\n" << *itr;
    }
    os << "\n]";
    return os;
}


//-------------------------------------------------------------------//
// Munkres
//-------------------------------------------------------------------//

template<typename Data> class Munkres
{
    static constexpr int NORMAL = 0;
    static constexpr int STAR   = 1;
    static constexpr int PRIME  = 2;
public:

    /*
     *
     * Linear assignment problem solution
     * [modifies matrix in-place.]
     * matrix(row,col): row major format assumed.
     *
     * Assignments are remaining 0 values
     * (extra 0 values are replaced with -1)
     *
     */
    void solve(Matrix<Data> &m) {
        const size_t rows = m.rows(),
                columns = m.columns(),
                size = XYZMAX(rows, columns);


        // Copy input matrix
        this->matrix = m;

        if ( rows != columns ) {
            // If the input matrix isn't square, make it square
            // and fill the empty values with the largest value present
            // in the matrix.
            matrix.resize(size, size, matrix.mmax());
        }


        // STAR == 1 == starred, PRIME == 2 == primed
        mask_matrix.resize(size, size);

        row_mask = new bool[size];
        col_mask = new bool[size];
        for ( size_t i = 0 ; i < size ; i++ ) {
            row_mask[i] = false;
        }

        for ( size_t i = 0 ; i < size ; i++ ) {
            col_mask[i] = false;
        }

        // Prepare the matrix values...

        // If there were any infinities, replace them with a value greater
        // than the maximum value in the matrix.
        replace_infinites(matrix);

        minimize_along_direction(matrix, rows >= columns);
        minimize_along_direction(matrix, rows <  columns);

        // Follow the steps
        int step = 1;
        while ( step ) {
            switch ( step ) {
                case 1:
                    step = step1();
                    // step is always 2
                    break;
                case 2:
                    step = step2();
                    // step is always either 0 or 3
                    break;
                case 3:
                    step = step3();
                    // step in [3, 4, 5]
                    break;
                case 4:
                    step = step4();
                    // step is always 2
                    break;
                case 5:
                    step = step5();
                    // step is always 3
                    break;
            }
        }

        // Store results
        for ( size_t row = 0 ; row < size ; row++ ) {
            for ( size_t col = 0 ; col < size ; col++ ) {
                if ( mask_matrix(row, col) == STAR ) {
                    matrix(row, col) = 0;
                } else {
                    matrix(row, col) = -1;
                }
            }
        }

        // Remove the excess rows or columns that we added to fit the
        // input to a square matrix.
        matrix.resize(rows, columns);

        m = matrix;

        delete [] row_mask;
        delete [] col_mask;
    }

    static void replace_infinites(Matrix<Data> &matrix) {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();
        //assert( rows > 0 && columns > 0 );
        double max = matrix(0, 0);
        constexpr auto infinity = std::numeric_limits<double>::infinity();

        // Find the greatest value in the matrix that isn't infinity.
        for ( size_t row = 0 ; row < rows ; row++ ) {
            for ( size_t col = 0 ; col < columns ; col++ ) {
                if ( matrix(row, col) != infinity ) {
                    if ( max == infinity ) {
                        max = matrix(row, col);
                    } else {
                        max = XYZMAX(max, matrix(row, col));
                    }
                }
            }
        }

        // a value higher than the maximum value present in the matrix.
        if ( max == infinity ) {
            // This case only occurs when all values are infinite.
            max = 0;
        } else {
            max++;
        }

        for ( size_t row = 0 ; row < rows ; row++ ) {
            for ( size_t col = 0 ; col < columns ; col++ ) {
                if ( matrix(row, col) == infinity ) {
                    matrix(row, col) = max;
                }
            }
        }

    }
    static void minimize_along_direction(Matrix<Data> &matrix, const bool over_columns) {
        const size_t outer_size = over_columns ? matrix.columns() : matrix.rows(),
                inner_size = over_columns ? matrix.rows() : matrix.columns();

        // Look for a minimum value to subtract from all values along
        // the "outer" direction.
        for ( size_t i = 0 ; i < outer_size ; i++ ) {
            double min = over_columns ? matrix(0, i) : matrix(i, 0);

            // As long as the current minimum is greater than zero,
            // keep looking for the minimum.
            // Start at one because we already have the 0th value in min.
            for ( size_t j = 1 ; j < inner_size && min > 0 ; j++ ) {
                min = XYZMIN(
                              min,
                              over_columns ? matrix(j, i) : matrix(i, j));
            }

            if ( min > 0 ) {
                for ( size_t j = 0 ; j < inner_size ; j++ ) {
                    if ( over_columns ) {
                        matrix(j, i) -= min;
                    } else {
                        matrix(i, j) -= min;
                    }
                }
            }
        }
    }

private:

    inline bool find_uncovered_in_matrix(const double item, size_t &row, size_t &col) const {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();

        for ( row = 0 ; row < rows ; row++ ) {
            if ( !row_mask[row] ) {
                for ( col = 0 ; col < columns ; col++ ) {
                    if ( !col_mask[col] ) {
                        if ( matrix(row,col) == item ) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    bool pair_in_list(const std::pair<size_t,size_t> &needle, const std::list<std::pair<size_t,size_t> > &haystack) {
        for ( std::list<std::pair<size_t,size_t> >::const_iterator i = haystack.begin() ; i != haystack.end() ; i++ ) {
            if ( needle == *i ) {
                return true;
            }
        }

        return false;
    }

    int step1() {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();

        for ( size_t row = 0 ; row < rows ; row++ ) {
            for ( size_t col = 0 ; col < columns ; col++ ) {
                if ( 0 == matrix(row, col) ) {
                    for ( size_t nrow = 0 ; nrow < row ; nrow++ )
                        if ( STAR == mask_matrix(nrow,col) )
                            goto next_column;

                    mask_matrix(row,col) = STAR;
                    goto next_row;
                }
                next_column:;
            }
            next_row:;
        }

        return 2;
    }

    int step2() {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();
        size_t covercount = 0;

        for ( size_t row = 0 ; row < rows ; row++ )
            for ( size_t col = 0 ; col < columns ; col++ )
                if ( STAR == mask_matrix(row, col) ) {
                    col_mask[col] = true;
                    covercount++;
                }

        if ( covercount >= matrix.minsize() ) {
            return 0;
        }


        return 3;
    }

    int step3() {
        /*
        Main Zero Search

         1. Find an uncovered Z in the distance matrix and prime it. If no such zero exists, go to Step 5
         2. If No Z* exists in the row of the Z', go to Step 4.
         3. If a Z* exists, cover this row and uncover the column of the Z*. Return to Step 3.1 to find a new Z
        */
        if ( find_uncovered_in_matrix(0, saverow, savecol) ) {
            mask_matrix(saverow,savecol) = PRIME; // prime it.
        } else {
            return 5;
        }

        for ( size_t ncol = 0 ; ncol < matrix.columns() ; ncol++ ) {
            if ( mask_matrix(saverow,ncol) == STAR ) {
                row_mask[saverow] = true; //cover this row and
                col_mask[ncol] = false; // uncover the column containing the starred zero
                return 3; // repeat
            }
        }

        return 4; // no starred zero in the row containing this primed zero
    }

    int step4() {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();

        // seq contains pairs of row/column values where we have found
        // either a star or a prime that is part of the ``alternating sequence``.
        std::list<std::pair<size_t,size_t> > seq;
        // use saverow, savecol from step 3.
        std::pair<size_t,size_t> z0(saverow, savecol);
        seq.insert(seq.end(), z0);

        // We have to find these two pairs:
        std::pair<size_t,size_t> z1(-1, -1);
        std::pair<size_t,size_t> z2n(-1, -1);

        size_t row, col = savecol;
        /*
        Increment Set of Starred Zeros

         1. Construct the ``alternating sequence'' of primed and starred zeros:

               Z0 : Unpaired Z' from Step 4.2
               Z1 : The Z* in the column of Z0
               Z[2N] : The Z' in the row of Z[2N-1], if such a zero exists
               Z[2N+1] : The Z* in the column of Z[2N]

            The sequence eventually terminates with an unpaired Z' = Z[2N] for some N.
        */
        bool madepair;
        do {
            madepair = false;
            for ( row = 0 ; row < rows ; row++ ) {
                if ( mask_matrix(row,col) == STAR ) {
                    z1.first = row;
                    z1.second = col;
                    if ( pair_in_list(z1, seq) ) {
                        continue;
                    }

                    madepair = true;
                    seq.insert(seq.end(), z1);
                    break;
                }
            }

            if ( !madepair )
                break;

            madepair = false;

            for ( col = 0 ; col < columns ; col++ ) {
                if ( mask_matrix(row, col) == PRIME ) {
                    z2n.first = row;
                    z2n.second = col;
                    if ( pair_in_list(z2n, seq) ) {
                        continue;
                    }
                    madepair = true;
                    seq.insert(seq.end(), z2n);
                    break;
                }
            }
        } while ( madepair );

        for ( std::list<std::pair<size_t,size_t> >::iterator i = seq.begin() ;
              i != seq.end() ;
              i++ ) {
            // 2. Unstar each starred zero of the sequence.
            if ( mask_matrix(i->first,i->second) == STAR )
                mask_matrix(i->first,i->second) = NORMAL;

            // 3. Star each primed zero of the sequence,
            // thus increasing the number of starred zeros by one.
            if ( mask_matrix(i->first,i->second) == PRIME )
                mask_matrix(i->first,i->second) = STAR;
        }

        // 4. Erase all primes, uncover all columns and rows,
        for ( size_t row = 0 ; row < mask_matrix.rows() ; row++ ) {
            for ( size_t col = 0 ; col < mask_matrix.columns() ; col++ ) {
                if ( mask_matrix(row,col) == PRIME ) {
                    mask_matrix(row,col) = NORMAL;
                }
            }
        }

        for ( size_t i = 0 ; i < rows ; i++ ) {
            row_mask[i] = false;
        }

        for ( size_t i = 0 ; i < columns ; i++ ) {
            col_mask[i] = false;
        }

        // and return to Step 2.
        return 2;
    }

    int step5() {
        const size_t rows = matrix.rows(),
                columns = matrix.columns();
        /*
        New Zero Manufactures

         1. Let h be the smallest uncovered entry in the (modified) distance matrix.
         2. Add h to all covered rows.
         3. Subtract h from all uncovered columns
         4. Return to Step 3, without altering stars, primes, or covers.
        */
        double h = 100000;//xyzoylz std::numeric_limits<double>::max();
        for ( size_t row = 0 ; row < rows ; row++ ) {
            if ( !row_mask[row] ) {
                for ( size_t col = 0 ; col < columns ; col++ ) {
                    if ( !col_mask[col] ) {
                        if ( h > matrix(row, col) && matrix(row, col) != 0 ) {
                            h = matrix(row, col);
                        }
                    }
                }
            }
        }

        for ( size_t row = 0 ; row < rows ; row++ ) {
            if ( row_mask[row] ) {
                for ( size_t col = 0 ; col < columns ; col++ ) {
                    matrix(row, col) += h;
                }
            }
        }

        for ( size_t col = 0 ; col < columns ; col++ ) {
            if ( !col_mask[col] ) {
                for ( size_t row = 0 ; row < rows ; row++ ) {
                    matrix(row, col) -= h;
                }
            }
        }

        return 3;
    }

    Matrix<int> mask_matrix;
    Matrix<Data> matrix;
    bool *row_mask;
    bool *col_mask;
    size_t saverow = 0, savecol = 0;
};


//-------------------------------------------------------------------//
// Matrix templateds
//-------------------------------------------------------------------//

/*export*/ template <class T>
Matrix<T>::Matrix() {
    m_rows = 0;
    m_columns = 0;
    m_matrix = nullptr;
}


/*export*/ template <class T>
Matrix<T>::Matrix(const std::initializer_list<std::initializer_list<T>> init) {
    m_matrix = nullptr;
    m_rows = init.size();
    if ( m_rows == 0 ) {
        m_columns = 0;
    } else {
        m_columns = init.begin()->size();
        if ( m_columns > 0 ) {
            resize(m_rows, m_columns);
        }
    }

    size_t i = 0, j;
    for ( auto row = init.begin() ; row != init.end() ; ++row, ++i ) {
        assert ( row->size() == m_columns && "All rows must have the same number of columns." );
        j = 0;
        for ( auto value = row->begin() ; value != row->end() ; ++value, ++j ) {
            m_matrix[i][j] = *value;
        }
    }
}

/*export*/ template <class T>
Matrix<T>::Matrix(const Matrix<T> &other) {
    if ( other.m_matrix != nullptr ) {
        // copy arrays
        m_matrix = nullptr;
        resize(other.m_rows, other.m_columns);
        for ( size_t i = 0 ; i < m_rows ; i++ ) {
            for ( size_t j = 0 ; j < m_columns ; j++ ) {
                m_matrix[i][j] = other.m_matrix[i][j];
            }
        }
    } else {
        m_matrix = nullptr;
        m_rows = 0;
        m_columns = 0;
    }
}

/*export*/ template <class T>
Matrix<T>::Matrix(const size_t rows, const size_t columns) {
    m_matrix = nullptr;
    resize(rows, columns);
}

/*export*/ template <class T>
Matrix<T> &
Matrix<T>::operator= (const Matrix<T> &other) {
    if ( other.m_matrix != nullptr ) {
        // copy arrays
        resize(other.m_rows, other.m_columns);
        for ( size_t i = 0 ; i < m_rows ; i++ ) {
            for ( size_t j = 0 ; j < m_columns ; j++ ) {
                m_matrix[i][j] = other.m_matrix[i][j];
            }
        }
    } else {
        // free arrays
        for ( size_t i = 0 ; i < m_columns ; i++ ) {
            delete [] m_matrix[i];
        }

        delete [] m_matrix;

        m_matrix = nullptr;
        m_rows = 0;
        m_columns = 0;
    }

    return *this;
}

/*export*/ template <class T>
Matrix<T>::~Matrix() {
    if ( m_matrix != nullptr ) {
        // free arrays
        for ( size_t i = 0 ; i < m_rows ; i++ ) {
            delete [] m_matrix[i];
        }

        delete [] m_matrix;
    }
    m_matrix = nullptr;
}

/*export*/ template <class T>
void
Matrix<T>::resize(const size_t rows, const size_t columns, const T default_value) {
    assert ( rows > 0 && columns > 0 && "Columns and rows must exist." );

    if ( m_matrix == nullptr ) {
        // alloc arrays
        m_matrix = new T*[rows]; // rows
        for ( size_t i = 0 ; i < rows ; i++ ) {
            m_matrix[i] = new T[columns]; // columns
        }

        m_rows = rows;
        m_columns = columns;
        clear();
    } else {
        // save array pointer
        T **new_matrix;
        // alloc new arrays
        new_matrix = new T*[rows]; // rows
        for ( size_t i = 0 ; i < rows ; i++ ) {
            new_matrix[i] = new T[columns]; // columns
            for ( size_t j = 0 ; j < columns ; j++ ) {
                new_matrix[i][j] = default_value;
            }
        }

        // copy data from saved pointer to new arrays
        size_t minrows = XYZMIN(rows, m_rows);
        size_t mincols = XYZMIN(columns, m_columns);
        for ( size_t x = 0 ; x < minrows ; x++ ) {
            for ( size_t y = 0 ; y < mincols ; y++ ) {
                new_matrix[x][y] = m_matrix[x][y];
            }
        }

        // delete old arrays
        if ( m_matrix != nullptr ) {
            for ( size_t i = 0 ; i < m_rows ; i++ ) {
                delete [] m_matrix[i];
            }

            delete [] m_matrix;
        }

        m_matrix = new_matrix;
    }

    m_rows = rows;
    m_columns = columns;
}

/*export*/ template <class T>
void
Matrix<T>::clear() {
    assert( m_matrix != nullptr );

    for ( size_t i = 0 ; i < m_rows ; i++ ) {
        for ( size_t j = 0 ; j < m_columns ; j++ ) {
            m_matrix[i][j] = 0;
        }
    }
}

/*export*/ template <class T>
inline T&
Matrix<T>::operator ()(const size_t x, const size_t y) {
    assert ( x < m_rows );
    assert ( y < m_columns );
    assert ( m_matrix != nullptr );
    return m_matrix[x][y];
}


/*export*/ template <class T>
inline const T&
Matrix<T>::operator ()(const size_t x, const size_t y) const {
    assert ( x < m_rows );
    assert ( y < m_columns );
    assert ( m_matrix != nullptr );
    return m_matrix[x][y];
}


/*export*/ template <class T>
const T
Matrix<T>::mmin() const {
    assert( m_matrix != nullptr );
    assert ( m_rows > 0 );
    assert ( m_columns > 0 );
    T min = m_matrix[0][0];

    for ( size_t i = 0 ; i < m_rows ; i++ ) {
        for ( size_t j = 0 ; j < m_columns ; j++ ) {
            min = std::min<T>(min, m_matrix[i][j]);
        }
    }

    return min;
}


/*export*/ template <class T>
const T
Matrix<T>::mmax() const {
    assert( m_matrix != nullptr );
    assert ( m_rows > 0 );
    assert ( m_columns > 0 );
    T max = m_matrix[0][0];

    for ( size_t i = 0 ; i < m_rows ; i++ ) {
        for ( size_t j = 0 ; j < m_columns ; j++ ) {
            max = std::max<T>(max, m_matrix[i][j]);
        }
    }

    return max;
}



//-------------------------------------------------------------------//
// NearestNeighborDistanceMetric
//-------------------------------------------------------------------//

// cosine distance between 2 vectors
Eigen::VectorXf _nn_cosine_distance(const FEATURESS &x,
                                    const FEATURESS &y){

    FEATURESS a = x;
    FEATURESS b = y;
    for (int row = 0; row < a.rows(); row++) {
        auto t = a.row(row);
        t = t / t.norm();
        a.row(row) = t;
    }
    for (int row = 0; row < b.rows(); row++) {
        auto t = b.row(row);
        t = t / t.norm();
        b.row(row) = t;
    }

    auto tmp = a*b.transpose();
    auto tmp1 = tmp.array();
    auto tmp2 = -(tmp1 - 1);
    DYNAMICM distances = tmp2.matrix();
    Eigen::VectorXf re(distances.cols());

    for (int col = 0; col < distances.cols(); col++) {
        auto cc = distances.col(col);
        float min = cc.minCoeff();
        re(col) = min;
    }
    return re;
}

// matching_threshold_
float NearestNeighborDistanceMetric::matching_threshold() {
    return matching_threshold_;
}

std::shared_ptr<NearestNeighborDistanceMetric> NearestNeighborDistanceMetric::Instance(){
    if(self_.get() == NULL){
        self_.reset(new NearestNeighborDistanceMetric());
    }
    return self_;
}

void NearestNeighborDistanceMetric::Init(float matching_threshold, int budget){
    matching_threshold_ = matching_threshold;
    budget_ = budget;
}


// Partial fit
void NearestNeighborDistanceMetric::partial_fit(const FEATURESS &features,
                                                const IDS &ids,
                                                const IDS &active_ids){

    for(int i = 0; i < features.rows(); i++){
        FEATURE feature = features.row(i);
        int iid = ids[i];

        {
            bool isIn = false;
            for(unsigned int k = 0; k < active_ids.size(); k++){
                if(iid == active_ids[k]){
                    isIn = true;
                    break;
                }
            }
            if(!isIn){
                continue;
            }
        }
        //
        std::map<int, std::vector<FEATURE>>::iterator it = samples_.find(iid);
        if(it == samples_.end()){
            std::vector<FEATURE> tmps;
            samples_.insert(std::make_pair(iid, tmps));
            it = samples_.find(iid);
        }
        it->second.push_back(feature);
        std::vector<FEATURE>::iterator ii = it->second.begin();
        if(it->second.size() > budget_){
            it->second.erase(ii);
        }
    }
}



DYNAMICM NearestNeighborDistanceMetric::distance(const FEATURESS &features, const IDS &ids){

    static DYNAMICM cost_matrix;
    cost_matrix = DYNAMICM(ids.size(), features.rows());
    DDS dds;

    for(unsigned int i = 0; i < ids.size(); i++){
        int iid = ids[i];
        std::vector<FEATURE> &ftsvec = samples_[iid];

        FEATURESS fts(ftsvec.size(), 256);//128
        for (unsigned int k = 0; k < ftsvec.size(); k++) {
            fts.row(k) = ftsvec[k];
        }
        Eigen::VectorXf dd = _nn_cosine_distance(fts, features);

        dds.Push(i, dd);
    }
    std::vector<std::pair<int, Eigen::VectorXf>> vec;
    dds.Get(vec);
    for(unsigned int i = 0; i < vec.size(); i++){
        std::pair<int, Eigen::VectorXf> pa = vec[i];
        cost_matrix.row(pa.first) =  pa.second;
    }
    return cost_matrix;
}
//-------------------------------------------------------------------//


//-------------------------------------------------------------------//
// HungarianOper
//-------------------------------------------------------------------//
Eigen::Matrix<float, -1, 2> HungarianOper::Solve(const DYNAMICM &cost_matrix) {
    int rows = cost_matrix.rows();
    int cols = cost_matrix.cols();
    Matrix<double> matrix(rows, cols);
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            matrix(row, col) = cost_matrix(row, col);
        }
    }

    Munkres<double> m;
    m.solve(matrix);

    std::vector<std::pair<int, int>> pairs;
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            int tmp = (int)matrix(row, col);
            if (tmp == 0) {
                std::pair<int, int> pa;
                pa.first = row;
                pa.second = col;
                pairs.push_back(pa);
            }
        }
    }


    int count = pairs.size();
    Eigen::Matrix<float, -1, 2> re(count, 2);
    for (int i = 0; i < count; i++) {
        std::pair<int, int> &pa = pairs[i];
        re(i, 0) = pa.first;
        re(i, 1) = pa.second;
    }
    return re;
}
//-------------------------------------------------------------------//


// KF filter
//-------------------------------------------------------------------//
bool KF::Init(){
    return true;
}


KF::KF(){
    int ndim = 4;
    double dt = 1.;

    _motion_mat_ = Eigen::MatrixXf::Identity(8, 8);
    for(int i = 0; i < ndim; i++){
        _motion_mat_(i, ndim + i) = dt;
    }
    _update_mat_ = Eigen::MatrixXf::Identity(4, 8);

    _std_weight_position_ = 1. / 20; // ori: 1. / 20
    _std_weight_velocity_ = 1. / 160;// ori: 1. / 160
}



VAR KF::Diag(const MEAN &mean) const{
    VAR var;
    for(int i = 0; i < var.rows(); i++){
        for(int j = 0; j < var.cols(); j++){
            if(i == j){
                var(i, j) = mean(i);
            }
            else{
                var(i, j) = 0;
            }
        }
    }
    return var;
}

NVAR KF::NDiag(const NMEAN &mean) const{
    NVAR var;
    for(int i = 0; i < var.rows(); i++){
        for(int j = 0; j < var.cols(); j++){
            if(i == j){
                var(i, j) = mean(i);
            }
            else{
                var(i, j) = 0;
            }
        }
    }
    return var;
}

std::pair<MEAN, VAR> KF::initiate(const DSBOX &measurement) const{
    DSBOX mean_pos = measurement;
    DSBOX mean_val;
    for(int i = 0; i < 4; i++){
        mean_val(i) = 0;
    }
    MEAN mean;
    for(int i = 0; i < 8; i++){
        if(i < 4){
            mean(i) = mean_pos(i);
            continue;
        }
        mean(i) = mean_val(i - 4);
    }

    MEAN std;
    std(0) = 2 * _std_weight_position_ * measurement[3];
    std(1) = 2 * _std_weight_position_ * measurement[3];
    std(2) = 1e-2;
    std(3) = 2 * _std_weight_position_ * measurement[3];
    std(4) = 10 * _std_weight_velocity_ * measurement[3];
    std(5) = 10 * _std_weight_velocity_ * measurement[3];
    std(6) = 1e-5;
    std(7) = 10 * _std_weight_velocity_ * measurement[3];


    MEAN tmp = std.array().square();
    VAR var = Diag(tmp);

    std::pair<MEAN, VAR> pa;
    pa.first = mean;
    pa.second = var;
    return pa;
}


std::pair<MEAN, VAR>  KF::predict(const MEAN &mean, const VAR &covariance) const{
    DSBOX std_pos;
    std_pos <<
            _std_weight_position_ * mean(3),
            _std_weight_position_ * mean(3),
            1e-2,
            _std_weight_position_ * mean(3);

    DSBOX std_vel;
    std_vel <<
            _std_weight_velocity_ * mean(3),
            _std_weight_velocity_ * mean(3),
            1e-5,
            _std_weight_velocity_ * mean(3);

    MEAN mtmp;
    for(int i = 0; i < 8; i++){
        if(i < 4){
            mtmp(i) = std_pos(i);
            continue;
        }
        mtmp(i) = std_vel(i - 4);
    }
    MEAN tmp = mtmp.array().square();
    VAR motion_cov = Diag(tmp);

    MEAN mean1 = _motion_mat_ * mean.transpose();

    VAR var = _motion_mat_ * covariance * (_motion_mat_.transpose());
    VAR var1 = var + motion_cov;

    std::pair<MEAN, VAR> pa;
    pa.first = mean1;
    pa.second = var1;
    return pa;
}

std::pair<MEAN, VAR>  KF::update(const MEAN &mean,  const VAR &covariance, const DSBOX &measurement) const{

    std::pair<NMEAN, NVAR> pa1 = _project(mean, covariance);
    NMEAN projected_mean = pa1.first;
    NVAR projected_cov = pa1.second;

    auto ddd = covariance * (_update_mat_.transpose());
    Eigen::Matrix<float, -1, 4> kalman_gain = projected_cov.llt().solve(ddd.transpose()).transpose(); // eg.8x4
    Eigen::Matrix<float, 1, 4> innovation = measurement - projected_mean; //eg.1x4


    auto tmp = innovation*(kalman_gain.transpose());
    MEAN new_mean = (mean.array() + tmp.array()).matrix();
    VAR new_covariance = covariance - kalman_gain*projected_cov*(kalman_gain.transpose());
    std::pair<MEAN, VAR> pa2;
    pa2.first = new_mean;
    pa2.second = new_covariance;
    return pa2;
}

Eigen::Matrix<float, 1, -1>  KF::gating_distance(const MEAN &meani, const VAR &covariance,
                                                 const DSBOXS &measurements, bool only_position) const{

    MEAN mean = meani;

    std::pair<NMEAN, NVAR>  pa1 = _project(mean, covariance);

    NMEAN mean1 = pa1.first;
    NVAR var1 = pa1.second;

    int count = measurements.rows();
    DSBOXS d(count, 4);
    for(int i = 0; i < count; i++){
        d.row(i) = measurements.row(i) - mean1;
    }


    Eigen::Matrix<float, -1, -1, Eigen::RowMajor> factor = var1.llt().matrixL();
    Eigen::Matrix<float, -1, -1> z = factor.triangularView<Eigen::Lower>().solve<Eigen::OnTheRight>(d).transpose();

    auto zz = ((z.array())*(z.array())).matrix();
    auto squared_maha = zz.colwise().sum();

    return squared_maha;

}

std::pair<NMEAN, NVAR>  KF::_project(const MEAN &mean, const VAR &covariance) const{
    NMEAN std;
    std <<
        _std_weight_position_ * mean[3],
            _std_weight_position_ * mean[3],
            1e-1,
            _std_weight_position_ * mean[3];
    NMEAN mtmp = std.array().square();

    NVAR innovation_cov = NDiag(mtmp);
    NMEAN mean1 = _update_mat_ * mean.transpose();


    NVAR var = _update_mat_ * covariance * (_update_mat_.transpose());
    NVAR var1 = var + innovation_cov;

    std::pair<NMEAN, NVAR> pa;
    pa.first = mean1;
    pa.second = var1;
    return pa;
};

//-------------------------------------------------------------------//
//KalmanTrackerN
//-------------------------------------------------------------------//
/*KalmanTrackerN::KalmanTrackerN(const MEAN &mean,
                               const VAR &covariance,
                               int tid,
                               int n_init,
                               int max_age,
                               const FEATURE &feature,
                               const Attribs &attribs,
                               bool featureFull, int oriPos){*/
KalmanTrackerN::KalmanTrackerN(const Detection &detection,
                               int tid,
                               int n_init,
                               int max_age,
                               const FEATURE &feature,
                               bool featureFull, int oriPos){

    this->track_id = tid;
    hits_ = 1;
    age_ = 1;
    time_since_update_ = 0;

    state_ = Tentative;
    if (featureFull) {
        features_.push_back(feature);
       // attribs_.push_back(attribs);
    }

    _n_init_ = n_init;
    _max_age_ = max_age;
    oriPos_ = oriPos;
    person << 0,0,0,0,0,0,0;

    kalmanFilter = kalmanP(new KF());

    std::pair<MEAN, VAR>  pa =
            kalmanFilter->initiate(detection.to_xyah());

    mean_ = pa.first;
    covariance_ = pa.second;

    ori_motion_mat_ = kalmanFilter->_motion_mat_;
    //tr_color =  trcolor;


}


DSBOX KalmanTrackerN::to_tlwh() const{
    DSBOX ret;
    ret(0) = mean_(0);
    ret(1) = mean_(1);
    ret(2) = mean_(2);
    ret(3) = mean_(3);
    ret(2) *= ret(3);
    ret(0) -= ret(2) / 2;
    ret(1) -= ret(3) / 2;

    ret(0) = max(1, min(int(ret(0)), 640));
    ret(1) = max(1, min(int(ret(1)), 480));

    return ret;
}

DSBOX KalmanTrackerN::to_tlbr(){
    DSBOX ret = to_tlwh();
    ret(2) = ret(0) + ret(2);
    ret(3) = ret(1) + ret(3);
    return ret;
}

DSBOX KalmanTrackerN::to_xcwh(){
    DSBOX ret;
    ret(0) = mean_(0);
    ret(1) = mean_(1);
    ret(2) = mean_(2);
    ret(3) = mean_(3);
    ret(2) *= ret(3);
    return ret;
}

void KalmanTrackerN::predict(){
    //Stores the previous position
    Rect tra;
    tra.x = mean_(0);
    tra.y = mean_(1);
    tra.width = mean_(2);
    tra.height =  mean_(3);
    tra.width *= mean_(3);
    tracklet.push_back(tra);


    if (time_since_update_ > 5){

        for(int i = 4; i < 6; i++){
            kalmanFilter->_motion_mat_(i, i) = kalmanFilter->_motion_mat_(i, i)*0.01;
        }
    }
    else if (time_since_update_ > 1) {
         for (int i = 6; i < 8; i++) {
                kalmanFilter->_motion_mat_(i, i) = kalmanFilter->_motion_mat_(i, i) * 0.001;
            }
        }
    else
        kalmanFilter->_motion_mat_ = ori_motion_mat_;
    //After predicts
    std::pair<MEAN, VAR> pa = kalmanFilter->predict(mean_, covariance_);
    mean_ = pa.first;
    covariance_ = pa.second;
    age_ += 1;
    time_since_update_ += 1;


}

void KalmanTrackerN::update(const Detection &detection){

    DSBOX box = detection.to_xyah();
    std::pair<MEAN, VAR> pa = kalmanFilter->update(
            mean_, covariance_, box);
    mean_ = pa.first;
    covariance_ = pa.second;

    features_.push_back(detection.feature_);
    //attribs_.push_back(detection.attribs_);

    hits_ += 1;
    time_since_update_ = 0;
    if (state_ == Tentative && hits_ >= _n_init_) {
        state_ = Confirmed;

    }
}


void KalmanTrackerN::mark_missed(){
    if(is_tentative()){
        state_ = Deleted;
    }
    else if(time_since_update_ > _max_age_){
        state_ = Deleted;
    }
}

bool KalmanTrackerN::check_id(){
    return person(0) == 0;
}


bool KalmanTrackerN::is_tentative(){
    return state_ == Tentative;
}

bool KalmanTrackerN::is_confirmed() const {
    return state_ == Confirmed;
}

bool KalmanTrackerN::is_deleted(){
    return state_ == Deleted;
}


PERSON KalmanTrackerN::is_in_dwell(vector<vector<Point2i>> dwell_regions) {
    DSBOX box = this->to_xcwh();
    //DSBOX backup_ = this->backup_to_xcwh();

    this->person(3) = box(0);
    this->person(4) = box(1);

    this->person(5) = tracklet.front().x;  //backup_(0);
    this->person(6) = tracklet.front().y; //backup_(1);

    if (dwell_regions.size() > 0) {
        for (size_t j = 0; j < dwell_regions.size(); j++) {
            //Calculate the number of apperances of this person to that region //
            if (pointPolygonTest(dwell_regions[j], Point2f(box(0), box(1)), false) == 1) {
                this->person(2) = this->person(2)+1;
                this->person(1) = j;
            }
        }
    }
    return this->person;
}


Point2i KalmanTrackerN::is_in_footfall(vector<Point2i> footfall_region){
    int in_footfall = 0;
    Point2i foot;

    foot.y = this->is_new;

    DSBOX box = this->to_xcwh();

    if (pointPolygonTest(footfall_region, Point2f(box(0), box(1)), false) == 1) {
            in_footfall = 1;
            this->is_new = 0;
        }
    foot.x = in_footfall;
    return foot;
}


void KalmanTrackerN::passed_entrance(vector<vector<Point2i>> entrance_lines, Mat &counters){
    DSBOX box = this->to_xcwh();
    // Examine to the current entrance line //
    for (size_t j = 0; j < entrance_lines.size(); j++) {
        int minx = min(entrance_lines[j][0].x, entrance_lines[j][1].x);
        int maxx = max(entrance_lines[j][0].x, entrance_lines[j][1].x);
        int miny = min(entrance_lines[j][0].y, entrance_lines[j][1].y);
        int maxy = max(entrance_lines[j][0].y, entrance_lines[j][1].y);

        // Horizontal entrance line //
        if ((maxx - minx) > (maxy - miny)) {
            int centery = int((maxy + miny) / 2);
            if ((box(0) >= minx) && (box(0) <= maxx) &&
                (tracklet.back().x >= minx) && (tracklet.back().x <= maxx)) {
                if ((box(1) >= centery) && (tracklet.back().y < centery)) {
                    // In //
                    counters.at<int>(j, 0) = counters.at<int>(j, 0) + 1;
                } else if ((box(1) < centery) && (tracklet.back().y >= centery)) {
                    // Out //
                    counters.at<int>(j, 1) = counters.at<int>(j, 1) + 1;
                }
            }
        }
            // Vertical entrance line //
        else {
            int centerx = int((maxx + minx) / 2);
            if ((box(1) >= miny) && (box(1) <= maxy) &&
                (tracklet.back().y >= miny) && (tracklet.back().y<= maxy)) {
                if ((box(0) >= centerx) && (tracklet.back().x < centerx)) {
                    // In //
                    counters.at<int>(j, 0) = counters.at<int>(j, 0) + 1;
                } else if ((box(0) < centerx) && (tracklet.back().x >= centerx)) {
                    // Out //
                    counters.at<int>(j, 1) = counters.at<int>(j, 1) + 1;
                }
            }
        }
    }
}

/*

void KalmanTrackerN::filter_tracklet(){
    cout<<filterlength<<endl;

   // for (size_t j = this->filterlength; j < tracklet.size(); j++) {
    if (tracklet.size()%10 == 0)
         {
             //cout<< "Traclet data:" <<  tracklet.at(j).y <<endl;
             tracklet_paint.push_back(tracklet.back());

             int st = int(tracklet_paint.size());
             if (st>3)
             {
                 //for (size_t jj = st; jj < tracklet_paint.size(); jj++){
                 float averageX = 0.;
                 float averageY = 0.;

                 for (size_t i = 0; i < 3; i++)
                 {
                     averageX += tracklet_paint.at(st-i-1).x / 3;
                     averageY += tracklet_paint.at(st-i-1).y / 3;
                 }
                 tracklet_paint.at(st-1).y = int(averageY);
                 tracklet_paint.at(st-1).x = int(averageX);
             }

         }

    this->filterlength = tracklet.size();
}
*/
/*
void KalmanTrackerN::filter_tracklet(){
    if (tracklet.size()>4)
    {
        for (size_t st = 2; st < tracklet.size()-2; st++) {
            float averageX = 0.;
            float averageY = 0.;

            averageX += tracklet.at(st).x / 5;
            averageX += tracklet.at(st-1).x / 5;
            averageX += tracklet.at(st-2).x / 5;
            averageX += tracklet.at(st+1).x / 5;
            averageX += tracklet.at(st+2).x / 5;

            averageY += tracklet.at(st).y / 5;
            averageY += tracklet.at(st-1).y / 5;
            averageY += tracklet.at(st-2).y / 5;
            averageY += tracklet.at(st+1).y / 5;
            averageY += tracklet.at(st+2).y / 5;

            tracklet.at(st).y = int(averageY);
            tracklet.at(st).x = int(averageX);
        }
    }
}*/

void KalmanTrackerN::moving_average(int start, int end, int kernel=5){

    for (size_t st = start; st < end; st++) {
        float averageX = 0.;
        float averageY = 0.;

        int  i = 0;
        int haf = int(kernel/2)+1;
        while(i < kernel){
            unsigned int ikk = (i<haf)? st - i : st + kernel -i;
            averageX += tracklet_paint.at(ikk).y / kernel;
            averageY += tracklet_paint.at(ikk).z / kernel;
            i++;
        }

        tracklet_paint.at(st).z = int(averageY);
        tracklet_paint.at(st).y = int(averageX);
    }

}



std::vector<Point3i>  KalmanTrackerN::filter_tracklet() {




    if (tracklet_paint.size()>0){
        float dist = sqrt(pow(tracklet.back().x- tracklet_paint.back().y, 2) + pow(tracklet.back().y - tracklet_paint.back().z, 2));
        cout<< "dist: " << dist <<endl;
        if (dist<3.) return tracklet_paint;
    }


  /*  if (filterlength==5 && tracklet_paint.size()>0)
   {
       //moving_average(filterlength, tracklet_paint.size() - 2, 5);
       int start = tracklet_paint.size();
       count++;
       for (size_t j = 2; j < 5; j++) {
           int index =  start - j;
           //cout<<"Index :" << index<<endl;
           tracklet_paint.erase(tracklet_paint.begin() + index);
       }
        this->filterlength = 0;
   }
   else {*/
   //tracklet_paint.push_back(tra);
   // this->filterlength++;
   //}
   // cout<<"Size :" << tracklet_paint.size()<<endl;

    // create a blank image (black image)


   /*
    tracklet.at(st).y = int(averageY);
    tracklet.at(st).x = int(averageX);*/
   if (tracklet_paint.size()  > 4 ) {
        Point3i tra;
        tra.x = person(0);

        float averageX = 0.;
        float averageY = 0.;

        averageX += tracklet.back().x / 4;
        averageX += tracklet.at(tracklet.size()-1).x / 4;
        averageX += tracklet.at(tracklet.size()-2).x/ 4;
        averageX += tracklet.at(tracklet.size()-3).x / 4;

        averageY += tracklet.back().y / 4;
        averageY += tracklet.at(tracklet.size()-1).y / 4;
        averageY += tracklet.at(tracklet.size()-2).y / 4;
        averageY += tracklet.at(tracklet.size()-3).y / 4;
        tra.z = int(averageY);
        tra.y = int(averageX);
        tracklet_paint.push_back(tra);
        //moving_average(tracklet_paint.size() - 5, tracklet_paint.size() - 2, 5);
    }else{
        Point3i tra;
        tra.x = person(0);
        tra.y = tracklet.back().x;
        tra.z = tracklet.back().y;
        tracklet_paint.push_back(tra);
    }

    return tracklet_paint;
}



//-------------------------------------------------------------------//
// TTracker
//-------------------------------------------------------------------//
TTracker::TTracker(float max_iou_distance, int max_age, int n_init){
    max_iou_distance_ = max_iou_distance;
    max_age_ = max_age;
    n_init_ = n_init;
    _next_id_ = 1;
    p = this;


}

NewAndDelete TTracker::update(const std::vector<Detection> &detections){
    NewAndDelete re;

    for(KalmanTracker kalmanTrack : kalmanTrackers_){
        kalmanTrack->predict();
    }

    RR rr = this->_match(detections);

    for(unsigned int i = 0; i < rr.matches.size(); i++){
        std::pair<int, int> pa = rr.matches[i];
        int track_idx = pa.first;
        int detection_idx = pa.second;
        kalmanTrackers_[track_idx]->update(detections[detection_idx]);
       }

    for(unsigned int i = 0; i < rr.unmatched_tracks.size(); i++){
        int track_idx = rr.unmatched_tracks[i];
        kalmanTrackers_[track_idx]->mark_missed();
    }

    //# -unmatches(detect)
    for(unsigned int i = 0; i < rr.unmatched_detections.size(); i++){
        int detection_idx = rr.unmatched_detections[i];
        //check if it is in footfall area
        int id = this->_NewTrack(detections[detection_idx]);
        re.news_.insert(std::make_pair(id, detections[detection_idx].oriPos_));
    }

    //Update Show IDs of The tracklets
    for(KalmanTracker t : kalmanTrackers_){
        if(t->is_confirmed()){
            if(t->check_id()){
                show_id = show_id + 1;
                t->person(0)  = show_id;
            }
        }
    }

    std::vector<KalmanTracker>::iterator it;
    while (1) {
        bool cn = false;
        for (it = kalmanTrackers_.begin(); it != kalmanTrackers_.end(); ++it) {
            KalmanTracker p = *it;
            if (p->is_deleted()) {
                re.deletes_.push_back(p->track_id);
                kalmanTrackers_.erase(it);
                //delete p;
                cn = true;
                break;
            }
        }
        if (cn) {
            continue;
        }
        break;
    }

    //# Update distance nearestNeighborDistanceMetric.
    IDS active_ids;
    for(KalmanTracker t : kalmanTrackers_){
        if(t->is_confirmed()){
            active_ids.push_back(t->track_id);
        }
    }

    int featureCount = 0;
    IDS ids;
    for(KalmanTracker t : kalmanTrackers_){
        if(!t->is_confirmed()){
            continue;
        }
        std::vector<FEATURE> &fts = t->features_;
        featureCount += fts.size();
        //ids += [kalmanTrack.track_id_ for _ in kalmanTrack.features_]
        for (unsigned int kk = 0; kk < fts.size(); kk++) {
            ids.push_back(t->track_id);
        }
    }

    //FEATURESS features(featureCount, 72);//128
    FEATURESS features(featureCount, 256);//128
    int pos = 0;
    for (KalmanTracker t : kalmanTrackers_) {
        if (!t->is_confirmed()) {
            continue;
        }
        std::vector<FEATURE> &fts = t->features_;
        for (unsigned int i = 0; i < fts.size(); i++) {
            FEATURE tt = fts.at(i);
            features.row(pos++) = tt;
        }
        t->features_.clear();
    }

    NearestNeighborDistanceMetric::Instance()->partial_fit(
            features, ids, active_ids);
    return re;
}


RR TTracker::_match(const std::vector<Detection> &detections){
    //Split track set into confirmed and unconfirmed kalmanTrackers.
    IDS confirmed_trackIds;
    IDS unconfirmed_trackIds;
    for(unsigned int i = 0; i < kalmanTrackers_.size(); i++){
        KalmanTracker t = kalmanTrackers_[i];
        if(t->is_confirmed()){
            confirmed_trackIds.push_back(i);
        }
        else{
            unconfirmed_trackIds.push_back(i);
        }
    }

    //# Associate confirmed kalmanTrackers using appearance features.
    RR rr = linear_assignment::matching_cascade(
            getCostMatrixByNND,
            NearestNeighborDistanceMetric::Instance()->matching_threshold(),
            max_age_,
            kalmanTrackers_,
            detections,
            &confirmed_trackIds);

    std::vector<std::pair<int, int> > matches_a = rr.matches;
    IDS unmatched_tracks_a = rr.unmatched_tracks;
    IDS unmatched_detections = rr.unmatched_detections;


    //# Associate remaining kalmanTrackers together with unconfirmed kalmanTrackers using IOU.
    IDS iou_track_candidateIds, tmp;
    std::copy(unconfirmed_trackIds.begin(),
              unconfirmed_trackIds.end(),
              std::back_inserter(iou_track_candidateIds));
    for(unsigned int k = 0; k < unmatched_tracks_a.size(); k++){
        int id = unmatched_tracks_a[k];
        if(kalmanTrackers_[id]->time_since_update_ == 1){
            iou_track_candidateIds.push_back(id);
        }
        else{
            tmp.push_back(id);
        }
    }
    unmatched_tracks_a.clear();
    unmatched_tracks_a = tmp;

    RR rr1 = linear_assignment::min_cost_matching(
            center_matching::getCostMatrixByCenter,
            max_iou_distance_,
            kalmanTrackers_,
            detections,
            &iou_track_candidateIds,
            &unmatched_detections);
    std::vector<std::pair<int, int> > matches_b = rr1.matches;
    IDS unmatched_tracks_b = rr1.unmatched_tracks;
    unmatched_detections = rr1.unmatched_detections;

    RR re;
    re.matches = matches_a;
    std::copy(matches_b.begin(), matches_b.end(),
              std::back_inserter(re.matches));
    re.unmatched_detections = unmatched_detections;
    re.unmatched_tracks = unmatched_tracks_a;
    std::copy(unmatched_tracks_b.begin(),
              unmatched_tracks_b.end(),
              std::back_inserter(re.unmatched_tracks));
    return re;
}

int TTracker::_NewTrack(const Detection &detection){
    int id = _next_id_;

    KalmanTracker newt(new KalmanTrackerN(
            detection, _next_id_, n_init_, max_age_,
            detection.feature_, true, detection.oriPos_));

    kalmanTrackers_.push_back(newt);
    _next_id_ += 1;
    return id;
}

DYNAMICM getCostMatrixByNND(const std::vector<KalmanTracker> &kalmanTrackers,
                            const std::vector<Detection> &dets,
                            IDS *track_indicesi,
                            IDS *detection_indicesi) {

    IDS track_indices = *track_indicesi;
    IDS detection_indices = *detection_indicesi;
    //FEATURESS features(detection_indices.size(), 72);//128
    FEATURESS features(detection_indices.size(), 256);//128
    for (unsigned int i = 0; i < detection_indices.size(); i++) {
        int pos = detection_indices[i];
        features.row(i) = dets[pos].feature_;
    }
    IDS ids;
    for (unsigned int i = 0; i < track_indices.size(); i++) {
        int pos = track_indices[i];
        ids.push_back(p->kalmanTrackers_[pos]->track_id);
    }
    DYNAMICM cost_matrix =
            NearestNeighborDistanceMetric::Instance()->distance(features, ids);


    DSBOXS measurements(detection_indices.size(), 4);

    for (unsigned int i = 0; i < detection_indices.size(); i++) {
        int pos = detection_indices[i];
        //DSBOX tmp = dets[pos].to_xyah();
        DSBOX tmp = dets[pos].tlwh_;
        measurements.row(i) = tmp;
    }

    DYNAMICM cost_matrix_1(track_indices.size(), detection_indices.size());
    //int ik =0;
    for (unsigned int row = 0; row < track_indices.size(); row++) {
        int track_idx = track_indices[row];
        /*if (p->kalmanTrackers_[track_idx]->time_since_update_ > 1) {
            for (int c = 0; c < cost_matrix.cols(); c++) {
                cost_matrix(row, c) = INFTY_COST;
            }
            continue;
        }*/

        DSBOX bbox = p->kalmanTrackers_[track_idx]->to_tlwh();
        Eigen::VectorXf tmpm1 = _centFunOut(bbox, measurements);
        auto tmp3 = tmpm1.array();
        cost_matrix_1.row(row) = tmp3.matrix();
    }

    for (unsigned int row = 0; row < track_indices.size(); row++) {
        cost_matrix.row(row) = 10*cost_matrix.row(row) + cost_matrix_1.row(row);
    }

    /*cost_matrix = linear_assignment::gate_cost_matrix(
            *KF::Instance(), cost_matrix, kalmanTrackers, dets, track_indices,
            detection_indices);*/


    return cost_matrix;
}


Eigen::VectorXf _centFunOut(const DSBOX &bbox, const DSBOXS &candidates){
    Eigen::VectorXf area_candidates(candidates.rows());
    Eigen::VectorXf re(candidates.rows());

    for(int i = 0; i < re.rows(); i++){
        DSBOX candidate = candidates.row(i);

        PT2 candidates_tl;
        candidates_tl(0, 0) = candidate[0];
        candidates_tl(0, 1) = candidate[1];

        re(i) = sqrt(pow( (bbox[0]/640 - candidates_tl(0, 0)/640),2)  + pow((bbox[1]/480 - candidates_tl(0, 1)/480),2)); //(bbox.rowwise() - candidates.row(i)).matrix().rowwise().norm();
        if(re(i)>0.25){
            re(i) = 1000.00;
        }
    }
    return re;
}

inline bool IsInRange(float val, float min, float max) {
    return min <= val && val <= max;
}

inline bool IsInRange(float val, cv::Vec2f range) {
    return IsInRange(val, range[0], range[1]);
}


Eigen::VectorXf center_matching::_iouFun(const DSBOX &bbox, const DSBOXS &candidates){
    Eigen::VectorXf area_candidates(candidates.rows());

    //
    PT2 bbox_tl; bbox_tl(0, 0) = bbox[0]; bbox_tl(0, 1) = bbox[1];
    PT2 bbox_br; bbox_br(0, 0) = (bbox[0] + bbox[2]); bbox_br(0, 1) = (bbox[1] + bbox[3]);
    DYNAMICM ctl(candidates.rows(), 2);
    DYNAMICM cbr(candidates.rows(), 2);
    for(int i = 0; i < candidates.rows(); i++){
        DSBOX candidate = candidates.row(i);
        PT2 candidates_tl;
        candidates_tl(0, 0) = candidate[0]; candidates_tl(0, 1) = candidate[1];
        ctl.row(i) = candidates_tl;
        PT2 candidates_br;
        candidates_br(0, 0) = (candidate[0] + candidate[2]);
        candidates_br(0, 1) = (candidate[1] + candidate[3]);
        cbr.row(i) = candidates_br;
        {
            area_candidates(i) = candidate[2] * candidate[3];
        }
    }


    DYNAMICM tl(candidates.rows(), 2);
    float btl0 = bbox_tl(0, 0);
    float btl1 = bbox_tl(0, 1);
    for (int i = 0; i < tl.rows(); i++) {
        float m = cv::max(btl0, ctl(i, 0));
        tl(i, 0) = m;
        m = cv::max(btl1, ctl(i, 1));
        tl(i, 1) = m;
    }

    DYNAMICM br(candidates.rows(), 2);
    float bbr0 = bbox_br(0, 0);
    float bbr1 = bbox_br(0, 1);
    for (int i = 0; i < br.rows(); i++) {
        br(i, 0) = cv::min(bbr0, cbr(i, 0));
        br(i, 1) = cv::min(bbr1, cbr(i, 1));
    }

    DYNAMICM wh(candidates.rows(), 2);
    Eigen::VectorXf area_intersection(candidates.rows());
    for (int i = 0; i < wh.rows(); i++) {
        for (int j = 0; j < wh.cols(); j++) {
            float tmp = br(i, j) - tl(i, j);
            wh(i, j) = tmp>0?tmp:0;
        }
        area_intersection(i) = wh(i, 0)*wh(i, 1);
    }

    float area_bbox = bbox(0, 2)*bbox(0, 3);

    Eigen::VectorXf re(candidates.rows());
    for (int i = 0; i < re.rows(); i++) {
        re(i) = area_intersection(i) / (area_bbox +
                                        area_candidates(i) -
                                        area_intersection(i));
    }
    return re;
}


DYNAMICM center_matching::getCostMatrixByCenter(const std::vector<KalmanTracker> &tracks,
                                          const std::vector<Detection> &detections,
                                          IDS *track_indicesi,
                                          IDS *detection_indicesi){
    IDS track_indices;
    if (track_indicesi == NULL) {
        for (unsigned int i = 0; i < tracks.size(); i++) {
            track_indices.push_back(i);
        }
    }
    else {
        track_indices = *track_indicesi;
    }

    IDS detection_indices;
    if (detection_indicesi == NULL) {
        for (unsigned int i = 0; i < detections.size(); i++) {
            detection_indices.push_back(i);
        }
    }
    else {
        detection_indices = *detection_indicesi;
    }
    DYNAMICM cost_matrix(track_indices.size(), detection_indices.size());
    for (unsigned int row = 0; row < track_indices.size(); row++) {
        int track_idx = track_indices[row];
        /* if (tracks[track_idx]->time_since_update_ > 1) {
            for (int c = 0; c < cost_matrix.cols(); c++) {
                cost_matrix(row, c) = INFTY_COST;
            }
            continue;
        }*/
        DSBOX bbox = tracks[track_idx]->to_tlwh();
        DSBOXS candidates(detection_indices.size(), 4);


        for (unsigned int k = 0; k < detection_indices.size(); k++) {
            DSBOX tmp = detections[detection_indices[k]].tlwh_;//.tlwh_
            candidates.row(k) = tmp;
        }

       /* Eigen::VectorXf tmpm = _iouFun(bbox, candidates);

        auto tmp1 = tmpm.array();
        auto tmp2 = -(tmp1 - 1);*/

        Eigen::VectorXf tmpm1 = _centFunOut(bbox, candidates);

        auto tmp3 = 10 * tmpm1.array();


        cost_matrix.row(row) = tmp3.matrix(); //tmp3.matrix() +
    }
    return cost_matrix;
}



//-------------------------------------------------------------------//
// Linear Assigment
//-------------------------------------------------------------------//


RR linear_assignment::min_cost_matching(
        const GetCostMarixFun &getCostMarixFun, float max_distance,
        const std::vector<KalmanTracker> &tracks,
        const std::vector<Detection> &detections,
        IDS *track_indicesi,
        IDS *detection_indicesi){

    IDS track_indices;
    IDS detection_indices;
    if (track_indicesi == NULL) {
        for (unsigned int i = 0; i < tracks.size(); i++) {
            track_indices.push_back(i);
        }
    }
    else {
        track_indices = *track_indicesi;
    }

    if (detection_indicesi == NULL) {
        for (unsigned int i = 0; i < detections.size(); i++) {
            detection_indices.push_back(i);
        }
    }
    else {
        detection_indices = *detection_indicesi;
    }

    if (detection_indices.empty() || track_indices.empty()) {
        RR rr;
        rr.unmatched_tracks = track_indices;
        rr.unmatched_detections = detection_indices;
        return rr;
    }

    DYNAMICM cost_matrix = getCostMarixFun(
            tracks, detections, &track_indices, &detection_indices);


    for (int i = 0; i < cost_matrix.rows(); i++) {
        for (int j = 0; j < cost_matrix.cols(); j++) {
            float tmp = cost_matrix(i, j);

            if (tmp > max_distance)  { // || isnan(tmp)
                cost_matrix(i, j) = 1000.;
            }
        }
    }

    Eigen::Matrix<float, -1, 2> indices =
            HungarianOper::Solve(cost_matrix);
    RR rr;

    for (unsigned int col = 0; col < detection_indices.size(); col++) {

        bool isIn = false;
        for (int i = 0; i < indices.rows(); i++) {
            unsigned int iid = indices(i, 1);
            if (col == iid) {
                isIn = true;
                break;
            }
        }
        if (!isIn) {
            int detection_idx = detection_indices[col];
            rr.unmatched_detections.push_back(detection_idx);
        }
    }

    for (unsigned int row = 0; row < track_indices.size(); row++) {

        bool isIn = false;
        for (int i = 0; i < indices.rows(); i++) {
            unsigned int iid = indices(i, 0);
            if (row == iid) {
                isIn = true;
                break;
            }
        }
        if (!isIn) {
            int track_idx = track_indices[row];
            rr.unmatched_tracks.push_back(track_idx);
        }
    }
    for (int i = 0; i < indices.rows(); i++) {
        int row = indices(i, 0);
        int col = indices(i, 1);
        int track_idx = track_indices[row];
        int detection_idx = detection_indices[col];
        if (cost_matrix(row, col) > max_distance) {
            rr.unmatched_tracks.push_back(track_idx);
            rr.unmatched_detections.push_back(detection_idx);
        }
        else {
            rr.matches.push_back(std::make_pair(track_idx, detection_idx));
        }
    }

    return rr;
}



RR linear_assignment::matching_cascade(
        const GetCostMarixFun &getCostMarixFun, float max_distance,
        int cascade_depth,
        const std::vector<KalmanTracker> &tracks,
        const std::vector<Detection> &detections,
        IDS *track_indicesi,
        IDS *detection_indicesi){
    IDS track_indices;
    IDS detection_indices;
    if(track_indicesi == NULL) {
        for (unsigned int i = 0; i < tracks.size(); i++) {
            track_indices.push_back(i);
        }
    }
    else {
        track_indices = *track_indicesi;
    }

    if (detection_indicesi == NULL) {
        for (unsigned int i = 0; i < detections.size(); i++) {
            detection_indices.push_back(i);
        }
    }
    else {
        detection_indices = *detection_indicesi;
    }


    RR re;
    std::map<int, int> tmpMap;
    IDS unmatched_detections = detection_indices;
   /* for (int level = 0; level < cascade_depth; level++) {
        if (unmatched_detections.empty()) {
            break;
        }
        IDS track_indices_l;
        for (unsigned int k = 0; k < track_indices.size(); k++) {
            if (tracks[k]->time_since_update_ == level + 1) {
                track_indices_l.push_back(track_indices[k]);
            }
        }
        if (track_indices_l.empty()) {
            continue;
        }


        RR rr = min_cost_matching(
                getCostMarixFun, max_distance, tracks, detections,
                &track_indices_l, &unmatched_detections);


        unmatched_detections = rr.unmatched_detections;
        for (unsigned int i = 0; i < rr.matches.size(); i++) {
            std::pair<int, int> pa = rr.matches[i];
            re.matches.push_back(pa);
            tmpMap.insert(pa);
        }
    }*/

   //Different from the original

    RR rr = min_cost_matching(
            getCostMarixFun, max_distance, tracks, detections,
            &track_indices, &unmatched_detections);

    unmatched_detections = rr.unmatched_detections;
    for (unsigned int i = 0; i < rr.matches.size(); i++) {
        std::pair<int, int> pa = rr.matches[i];
        re.matches.push_back(pa);
        tmpMap.insert(pa);
    }

    //edoooooooooooooooooo

    re.unmatched_detections = unmatched_detections;
    for (unsigned int i = 0; i < track_indices.size(); i++) {
        int tid = track_indices[i];
        std::map<int, int>::iterator it = tmpMap.find(tid);
        if (it == tmpMap.end()) {
            re.unmatched_tracks.push_back(tid);
        }
    }

    return re;
}

//We do not use them. I removed them from the matching process.
DYNAMICM linear_assignment::gate_cost_matrix(
        const KF &kalmanFilter,
        DYNAMICM &cost_matrix,
        const std::vector<KalmanTracker> &tracks,
        const std::vector<Detection> &detections,
        IDS track_indices,
        IDS detection_indices,
        int gated_cost,
        bool only_position){
    int gating_dim = only_position ? 2 : 7;
    float gating_threshold = chi2inv95[gating_dim];
    DSBOXS measurements(detection_indices.size(), 4);

    for (unsigned int i = 0; i < detection_indices.size(); i++) {
        int pos = detection_indices[i];
        DSBOX tmp = detections[pos].to_xyah();
        measurements.row(i) = tmp;
    }

    for (unsigned int row = 0; row < track_indices.size(); row++) {
        int track_idx = track_indices[row];
        KalmanTracker track = tracks[track_idx];
        // gating_distance is a vector
        Eigen::Matrix<float, 1, -1> gating_distance = kalmanFilter.gating_distance(
                track->mean_, track->covariance_, measurements, only_position);
        for (int i = 0; i < gating_distance.cols(); i++) {
            if (gating_distance(0, i) > gating_threshold) {
                cost_matrix(row, i) = gated_cost;
            }
        }
    }
    return cost_matrix;
}

//-------------------------------------------------------------------//

//-------------------------------------------------------------------//
PedestrianTracker::PedestrianTracker(const TrackerParams params) //, const TrackerParams &params
    :params_(params)
    {
        ValidateParams(params_);
        tt_ = TTrackerP(new TTracker(params_.max_iou_distance, params_.max_age, params_.FRAMES_INIT));
        NearestNeighborDistanceMetric::Instance()->Init(params_.reid_thr, params_.bugdet);
    }


void PedestrianTracker::Process(const cv::Mat &frame, std::vector<Detection> &input_detections,
                                int frame_idx, vector<vector<int>> &footfall_id) {

    detect_ = input_detections;
    // Process the Input Detections
    tt_->update(input_detections);

    std::vector<KalmanTracker> &kalmanTrackers = tt_->kalmanTrackers_;

    //initialize the footfall counter and the vector dwell counter
    for (size_t i = 0; i < params_.FOOTFALL_REGIONS.size(); i++)
        footfall_counters.at<int>(i, 0) = 0;
    dwell_counters.clear();

    tracklet_paint_.clear();

    for (const auto &track :kalmanTrackers) {
        if (!track->is_confirmed())// || (track->time_since_update_ > 0)
            continue;

        // check if track is in dwell
        PERSON dwell_counter = track->is_in_dwell(params_.DWELL_REGIONS);
        dwell_counters.push_back(dwell_counter);

        // check if track is in footfall region
        for (size_t i = 0; i < params_.FOOTFALL_REGIONS.size(); i++) {
            vector<Point2i> foot_reg = params_.FOOTFALL_REGIONS[i];
            Point2i in_foot = track->is_in_footfall(foot_reg);

            int in_footfall = in_foot.x;
            int is_new = in_foot.y;

            if (in_footfall && is_new) {
                footfall_counters.at<int>(i, 0) = footfall_counters.at<int>(i, 0) + 1; // The current counter //
                footfall_counters.at<int>(i, 1) = footfall_counters.at<int>(i, 1) + 1; // The overall counter //
            } else if (in_footfall) {
                footfall_counters.at<int>(i, 0) = footfall_counters.at<int>(i, 0) + 1; // The current counter //
            } else;
        }

        tracklet_paint_.push_back(track->filter_tracklet());

        /*
        DSBOX box = track->to_tlwh();
        int id = track->person(0);

        Rect rc;
        rc.x = box(0);
        rc.y = box(1);
        rc.width = box(2);
        rc.height = box(3);
        Scalar clr = cv::Scalar(0, 255, 0);
        rectangle(frame, rc, clr);
        std::string disp = toStr(id);
        cv::putText(frame,
                    disp,
                    cv::Point(rc.x, rc.y),
                    cv::FONT_HERSHEY_SIMPLEX,
                    0.6,
                    cv::Scalar(0, 0, 255));
        //imshow("DeepSort", frame);
        //waitKey(1);*/

        // check if track has passed the entrance
        track->passed_entrance(params_.ENTRANCE_LINES, entrance_counters);
    }
};

//////////////////////////////////////////////////////////////////////////////////
///////////////////////// Check the set of ROIS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::vector2point(vector<vector<vector<int>>> data_in, vector<vector<cv::Point>> &data_out){
    vector<vector<Point>> datai;
    for (size_t i = 0; i < data_in.size(); i++){
        vector<Point> dataj;
        for (size_t j = 0; j < data_in.at(i).size(); j++){
            dataj.push_back(Point(data_in.at(i).at(j).at(0),data_in.at(i).at(j).at(1)));
        }
        datai.push_back(dataj);
    }
    // Output vector //
    data_out = datai;
}


//////////////////////////////////////////////////////////////////////////////////
///////////////////////// ReadREGIONS //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::ReadREGIONS(vector<vector<vector<int>>> footfallREG_v, vector<vector<vector<int>>> dwellREG_v, vector<vector<vector<int>>> entranceLIN_v){

    vector2point(footfallREG_v,  params_.FOOTFALL_REGIONS);
    vector2point(dwellREG_v,  params_.DWELL_REGIONS);
    vector2point(entranceLIN_v,  params_.ENTRANCE_LINES);

    entrance_counters = Mat::zeros(entranceLIN_v.size(), 2, CV_32SC1);
    footfall_counters = Mat::zeros(footfallREG_v.size(), 2, CV_32SC1);
}






//////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Define the color ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DefineColor(string index, Mat &color){
    if (index.compare("yellow") == 0){ color.at<int>(0, 0) = 0; color.at<int>(0, 1) = 255; color.at<int>(0, 2) = 255; }
    else if (index.compare("magenta") == 0){ color.at<int>(0, 0) = 255; color.at<int>(0, 1) = 0; color.at<int>(0, 2) = 255; }
    else if (index.compare("cyan") == 0){ color.at<int>(0, 0) = 255; color.at<int>(0, 1) = 255; color.at<int>(0, 2) = 0; }
    else if (index.compare("red") == 0){ color.at<int>(0, 0) = 0; color.at<int>(0, 1) = 0; color.at<int>(0, 2) = 255; }
    else if (index.compare("green") == 0){ color.at<int>(0, 0) = 0; color.at<int>(0, 1) = 255; color.at<int>(0, 2) = 0; }
    else if (index.compare("blue") == 0){ color.at<int>(0, 0) = 255; color.at<int>(0, 1) = 0; color.at<int>(0, 2) = 0; }
    else if (index.compare("white") == 0){ color.at<int>(0, 0) = 255; color.at<int>(0, 1) = 255; color.at<int>(0, 2) = 255; }
    else { color.at<int>(0, 0) = 0; color.at<int>(0, 1) = 0; color.at<int>(0, 2) = 0; }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////// Draw the set of points in the image plane ///////////////////////
//////////////////////////////////////////////////////////////////////////////////
/*void PedestrianTracker::DrawPoints(Mat im, vector<PERSON> people, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        for (auto person : people) {
            circle(im, Point(person(3), person(4)), 3,
                   Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), -1);
        }
    }
}*/

void PedestrianTracker::DrawPoints(Mat im, vector<vector<Point3i>> coords, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        //for (auto person : people) {
        if (coords.size() > 0){
            for (size_t i = 0; i < coords.size(); i++){
                //for (size_t j = 0; j < coords[i].size() - 1; j++) {
                    circle(im, Point(coords[i].back().y, coords[i].back().z), 3,
                         Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), -1);
               // }
            }
       }
    }
}



//////////////////////////////////////////////////////////////////////////////////
////////////////// Draw the set of lines in the image plane //////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawLines(Mat im, vector<vector<Point2i>> coords, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        if (coords.size() > 0){
            for (size_t i = 0; i < coords.size(); i++){
                line(im, coords[i][0], coords[i][1], Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////
///////////////// Draw the set of regions in the image plane /////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawRectangles(Mat im, std::vector<Detection> detect, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);

        for (const auto &detection : detect) {
            Rect rc;
            rc.x = detection.tlwh_(0);
            rc.y = detection.tlwh_(1);
            rc.width = detection.tlwh_(2);
            rc.height = detection.tlwh_(3);
            rectangle(im, rc,  Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
        }

    }
}


//////////////////////////////////////////////////////////////////////////////////
///////////////// Draw the set of polygons in the image plane /////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawPolygons(Mat im, vector<vector<Point2i>> coords, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        if (coords.size() > 0){
            for (size_t i = 0; i < coords.size(); i++){
                for (size_t j = 0; j < coords[i].size() - 1; j++){
                    line(im, coords[i][j], coords[i][j + 1], Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
                }
                line(im, coords[i][coords[i].size() - 1], coords[i][0], Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
            }
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////
///////////////// Draw the set of polygons in the image plane /////////////////////
//////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Draw the entrance counters ///////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawEntranceCounters(Mat im, Mat counters, string set_color, bool enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        // Initialize the headers //
        float txt_size = float(im.rows) / float(1080);
        string text;
        char const *pchar = text.c_str();
        float txt_start_x_In, txt_start_y_In;
        float txt_start_x_Out, txt_start_y_Out;
        int baseline = 0;
        Size textSize_In, textSize_Out;
        // Draw the Headers of the counters //
        text = "Entrances In : ";
        textSize_In = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
        txt_start_x_In = 0; txt_start_y_In = 10;
        putText(im, text, Point(txt_start_x_In, txt_start_y_In), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);

        text = "Entrances Out: ";
        textSize_Out = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
        txt_start_x_Out = 0; txt_start_y_Out = txt_start_y_In + 10 + textSize_In.height;
        putText(im, text, Point(txt_start_x_Out, txt_start_y_Out), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);

        for (int i = 0; i < counters.rows; i++){
            // Draw the counters of the In movement //
            if (i == counters.rows - 1){
                text = to_string(counters.at<int>(i, 0));
            }
            else{
                text = to_string(counters.at<int>(i, 0)) + ", ";
            }
            txt_start_x_In = txt_start_x_In + textSize_In.width;
            textSize_In = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
            putText(im, text, Point(txt_start_x_In, txt_start_y_In), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
            // Draw the counters of the Out movement //
            if (i == counters.rows - 1){
                text = to_string(counters.at<int>(i, 1));
            }
            else{
                text = to_string(counters.at<int>(i, 1)) + ", ";
            }
            txt_start_x_Out = txt_start_x_Out + textSize_Out.width;
            textSize_Out = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
            putText(im, text, Point(txt_start_x_Out, txt_start_y_Out), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Draw the footfall counters ///////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawFootfallCounters(Mat im, Mat counters, string set_color, bool enable, bool second_enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        // Initialize the headers //
        float txt_size = float(im.rows) / float(1080);
        string text;
        char const *pchar = text.c_str();
        float txt_start_x_In, txt_start_y_In;
        float txt_start_x_Out, txt_start_y_Out;
        int baseline = 0;
        Size textSize_In, textSize_Out;
        // Draw the Headers of the counters //
        text = "Footfalls Current : ";
        textSize_In = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
        txt_start_x_In = 0;
        if (second_enable){
            txt_start_y_In = textSize_In.height + 10 + textSize_In.height + 10 + textSize_In.height;
        }
        else{
            txt_start_y_In = 10;
        }
        putText(im, text, Point(txt_start_x_In, txt_start_y_In), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);

        text = "Footfalls Overall: ";
        textSize_Out = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
        txt_start_x_Out = 0;
        if (second_enable){
            txt_start_y_Out = txt_start_y_In + 10 + textSize_In.height;
        }
        else{
            txt_start_y_Out = txt_start_y_In + 10 + textSize_In.height;
        }
        putText(im, text, Point(txt_start_x_Out, txt_start_y_Out), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);

        for (int i = 0; i < counters.rows; i++){
            // Draw the counters of the In movement //
            if (i == counters.rows - 1){
                text = to_string(counters.at<int>(i, 0));
            }
            else{
                text = to_string(counters.at<int>(i, 0)) + ", ";
            }
            txt_start_x_In = txt_start_x_In + textSize_In.width;
            textSize_In = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
            putText(im, text, Point(txt_start_x_In, txt_start_y_In), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
            // Draw the counters of the Out movement //
            if (i == counters.rows - 1){
                text = to_string(counters.at<int>(i, 1));
            }
            else{
                text = to_string(counters.at<int>(i, 1)) + ", ";
            }
            txt_start_x_Out = txt_start_x_Out + textSize_Out.width;
            textSize_Out = getTextSize(text, FONT_HERSHEY_TRIPLEX, txt_size, 1, &baseline);
            putText(im, text, Point(txt_start_x_Out, txt_start_y_Out), FONT_HERSHEY_TRIPLEX, txt_size, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);
        }
    }
}


/*Mat draw_track(Mat frame, bool enable){

    string text;
    char const *pchar = text.c_str();

    int j=0;

    if ( tracklet_paint_.size()>0 && enable)
    {
        for (size_t i = 0; i < tracklet_paint_.size()-1; i++){
            Point2i tra = tracklet_paint_.at(i);
            Point2i tra_next = tracklet_paint_.at(i+1);
            line(frame, tra, tra_next, tr_color, 1);
            j += 5;
        }
        line(frame, tracklet_paint_.back() , Point(mean_(0), mean_(1)), tr_color, 5);
    }else
        line(frame, tracklet.back() , Point(mean_(0), mean_(1)), tr_color, 5);

    text = to_string(person(0));
    putText(frame, pchar, Point(mean_(0) + 10,  mean_(1) + 5), FONT_HERSHEY_TRIPLEX, 1.0, tr_color, 2);

    return frame;
}*/

//////////////////////////////////////////////////////////////////////////////////
/////////////// Draw the tracks for the set of detected people ///////////////////
//////////////////////////////////////////////////////////////////////////////////

void PedestrianTracker::DrawTracks(Mat im, vector<vector<Point3i>> coords, bool enable){
    if (enable){
        // Extract the specified color //
        string text;
        char const *pchar = text.c_str();


        if (coords.size()>0) {
            for (size_t i = 0; i < coords.size(); i++) {
                if (coords[i].size() > 1) {
                    // Draw the track of the person //
                    for (size_t j = 0; j < coords[i].size() - 1; j++) {
                        line(im, Point(coords[i][j].y, coords[i][j].z), Point(coords[i][j + 1].y, coords[i][j + 1].z),
                             params_.colors_.at(int(coords[i][0].x)), 2);
                    }

                    text = to_string(coords[i][0].x);
                    putText(im, pchar, Point(coords[i].back().y + 10, coords[i].back().z + 5), FONT_HERSHEY_TRIPLEX,
                            1.0,
                            params_.colors_.at(int(coords[i][0].x)), 2);

                }
            }
        }
            // create hull array for convex hull points
           /* vector<vector<Point>> contour;

            contour.clear();

            vector<Point> cont;
            if (coords.size()>2){
                //contour.clear();
                //cont.clear();
                for (size_t i = 0; i < coords.size(); i++) {
                    for (size_t j = 0; j < coords[i].size() - 1; j++) {
                        Point poin;
                        poin.x = coords[i][j].y;
                        poin.y = coords[i][j].z;
                        cont.push_back(poin);
                    }
                    contour.push_back(cont);
                }
            }*/
          /*  vector<vector<Point>> hull(contour.size());

            for(int i = 0; i < contour.size(); i++) {
                cout << "edo" << contour[i] << endl;
                convexHull(Mat(contour[i]), hull[i], false);
            }

           cout<<"edo1"<<endl;
            for(int i = 0; i < coords.size(); i++) {
                Scalar color_contours = Scalar(0, 255, 0); // green - color for contours
                Scalar color = Scalar(255, 0, 0); // blue - color for convex hull
                // draw ith contour
                //drawContours(im, contours, i, color_contours, 1, 8, vector<Vec4i>(), 0, Point());
                // draw ith convex hull
                drawContours(im, hull[i], i, color, 1, 8, vector<Vec4i>(), 0, Point());
            }
                cout<<"edo2"<<endl;
         }*/

    }
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////// Draw the dwell counters /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void PedestrianTracker::DrawDwellCounters(Mat im, vector<PERSON> counters, string set_color, bool enable, bool second_enable){
    if (enable){
        // Extract the specified color //
        Mat color = Mat::zeros(1, 3, CV_32SC1);
        DefineColor(set_color, color);
        string text, text_draw;
        char const *pchar = text.c_str();
        float txt_start;
        int baseline = 0;
        Size textSize;


        for (auto count : counters){
            //       count = counters(i);
            // Draw the value of the dwell counter for that person (and the ID of the dwell) //

            text = "[" + to_string(count(0)) + "," + to_string(count(2)) + "]";
            // Correspond the dwell to that person //
            text_draw = to_string(count(0));
            // Get the size of the ID //
            textSize = getTextSize(text_draw, FONT_HERSHEY_TRIPLEX, 0.5, 1, &baseline);
            if (second_enable){
                txt_start = 15 + textSize.width;
            }
            else{
                txt_start = 15;
            }
            putText(im, pchar, Point(count(3) + txt_start, count(4) + 5), FONT_HERSHEY_TRIPLEX, 0.5, Scalar(color.at<int>(0, 0), color.at<int>(0, 1), color.at<int>(0, 2)), 1);

        }
    }
}



Mat PedestrianTracker::Visualize(int frame_index, Mat im){

    DrawLines(im, params_.ENTRANCE_LINES, params_.ENTRANCE_LINES_COLOR, params_.DRAW_ENTRANCE_LINES);

    // Draw the footfall regions //
    DrawPolygons(im, params_.FOOTFALL_REGIONS, params_.FOOTFALL_REGIONS_COLOR, params_.DRAW_FOOTFALL_REGIONS);

    // Draw the dwell regions //
    DrawPolygons(im, params_.DWELL_REGIONS, params_.DWELL_REGIONS_COLOR, params_.DRAW_DWELL_REGIONS);

    // Draw the detected bounding boxes //
    DrawRectangles(im, detect_, params_.BOUNDING_BOXES_COLOR, params_.DRAW_BOUNDING_BOXES);

    // Draw the detected people //
    // DrawPoints(im, dwell_counters, params_.CENTROIDS_COLOR, params_.DRAW_CENTROIDS);
    DrawPoints(im, tracklet_paint_, params_.CENTROIDS_COLOR, params_.DRAW_CENTROIDS);

    // Draw the tracks of the detected people //
    DrawTracks(im, tracklet_paint_, params_.DRAW_TRACKS);

    // Draw the counters of the entrance lines //
    DrawEntranceCounters(im, entrance_counters, params_.ENTRANCE_COUNTERS_COLOR, params_.DRAW_ENTRANCE_COUNTERS);

    // Draw the counters of the footfall regions //
    DrawFootfallCounters(im, footfall_counters, params_.FOOTFALL_COUNTERS_COLOR, params_.DRAW_FOOTFALL_COUNTERS, params_.DRAW_ENTRANCE_COUNTERS);

    // Draw the counters of the dwell regions //
    DrawDwellCounters(im, dwell_counters, params_.DWELL_COUNTERS_COLOR, params_.DRAW_DWELL_COUNTERS,  params_.DRAW_TRACKS);

    // Draw the image //
    if (params_.DRAW_IMAGE) {
        //cv::resize(im, im, cv::Size(), 2, 2); //320 x 240 -> 640 x 480
        imshow("Tracker", im);
        waitKey(1);
    }

    // Audit operation //
    if(params_.AUDIT_MODE){
        string name = "/home/sofoklis/Pictures/NewPeopleTrLib/DeepSort/Output_Frames/frame_" + to_string(frame_index) + ".jpg";
        imwrite(name, im);
    }

    return im;
};