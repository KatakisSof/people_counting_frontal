#include "readVisualConfig.hpp"

VisualConfigClass::VisualConfigClass(std::string filename)
{
    verbosity = verb;
    data = LoadConfig(  filename);
}

int VisualConfigClass::get_int(const char * var){
    auto entry = data.find(var);
    if(entry != data.end())
    {
        return  std::stoi(entry->second);
    }
    return -1;
   
}

bool VisualConfigClass::get_bool(const char * var){
    auto entry = data.find(var);
    if(entry != data.end())
    {
        std::transform(entry->second.begin(), entry->second.end(), entry->second.begin(), ::tolower);
        return  (entry->second.compare("true")==0);
    }
    return false;
}

std::string VisualConfigClass::get_string(const char * var){
    auto entry = data.find(var);
    if(entry != data.end())
    {
        return  entry->second;
    }

    return std::string("");
}


std::map<std::string,std::string> VisualConfigClass::LoadConfig(std::string filename)
{
    std::ifstream input(filename.c_str()); //The input stream
    std::map<std::string,std::string> ans; //A map of key-value pairs in the file
    while(input) //Keep on going as long as the file stream is good
    {
        std::string key;  
        std::string value;  
        std::getline(input, key, ':'); //Read up to the : delimiter into key
        std::getline(input, value, '\n'); //Read up to the newline into value
        std::string::size_type pos1 = value.find_first_of("\""); //Find the first quote in the value
        std::string::size_type pos2 = value.find_last_of("\""); //Find the last quote in the value
        if(pos1 != std::string::npos && pos2 != std::string::npos && pos2 > pos1) //Check if the found positions are all valid
        {
            value = value.substr(pos1+1,pos2-pos1-1); //Take a substring of the part between the quotes
            ans[key]  =value;
            if(verbosity!=0){
                std::cout << "key = " << key << std::endl;
                std::cout << "value = " << value << std::endl;
            }
        }
    }
    input.close(); //Close the file stream
    return ans; //And return the result
}
