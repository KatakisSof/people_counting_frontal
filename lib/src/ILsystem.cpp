#include "ILsystem.hpp"



ILsystem::ILsystem(string jsonfile,  vector<string> Keys, bool Verbose=false){
  
  Verbosity = Verbose;
  read_entries( jsonfile, Keys);
  
}

void ILsystem::read_entries(string jsonfile, vector<string> Keys){

   ifstream ifs(jsonfile);
   auto js = json::parse(ifs);
 
   string s = js.dump();     
   for(size_t n =0; n < Keys.size(); n++)
   {
		if(Verbosity){
		cout << js[Keys.at(n)] << endl;
		}
	   for(size_t i =0; i < js[Keys.at(n)].size() ; i++)
	   {     
			vector<vector<int>> entry;
			for(size_t j =0; j < js[Keys.at(n)].at(i).size() ; j++)
			{   
				vector<int> onepoint;
				onepoint.push_back(js[Keys.at(n)].at(i).at(j).at(0).get<int>());
				onepoint.push_back(js[Keys.at(n)].at(i).at(j).at(1).get<int>());
				entry.push_back(onepoint);
			}
			pdata.push_back(entry);      
	  }
	  // add entry
	  data.insert(make_pair(Keys.at(n), pdata));
    }
}

vector<vector<vector<int>>> ILsystem::get_polygon(string query){
   
    return data[query];

}
void ILsystem::print_polygon(string query){
   
	cout << " Printing information about polygon: " << query << endl;
	cout << "Number of entries:" << data[query].size() << endl;
	for(size_t i=0; i< data[query].size() ;i++){
		for(size_t j=0; j < data[query].at(i).size(); j++){
		   cout<< "x: " << data[query].at(i).at(j).at(0) << ", y: " << data[query].at(i).at(j).at(1);
		}
		cout << " " << endl;
	} 
}

void ILsystem::save_tracks(vector<Point3i> people, int frame){
	char bufName[32];
	snprintf(bufName, sizeof(char)*32, "../Output/frame_%06i.txt", frame); 
	fstream outputFile;
	outputFile.open(bufName, ios::out);
	for (size_t i=0; i<people.size(); i++){
		outputFile << people[i].x << " " << people[i].y << " " << people[i].z << endl;
	}
	outputFile.close();
}
