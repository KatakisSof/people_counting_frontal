// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

/**
* \brief The entry point for the Inference Engine interactive_Vehicle_detection demo application
* \file security_barrier_camera_demo/main.cpp
* \example security_barrier_camera_demo/main.cpp
*/
#include "ILTracker.hpp"
#include "ILDetector.hpp"
#include "ILsystem.hpp"
#include "image_reader.hpp"
#include <ie_plugin_config.hpp>
#include <stdio.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <samples/ocv_common.hpp>
#include <iostream>
#include <utility>
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <gflags/gflags.h>

#include <fstream>
#include <ctype.h>
#include <ctime>


// OpenNI
#include <OpenNI.h>
#include "OniSampleUtilities.h"

using namespace std;
using namespace openni;
using namespace InferenceEngine;

using ImageWithFrameIndex = std::pair<cv::Mat, int>;

const std::string json_Path = "/home/sofoklis/Pictures/NewPeopleTrLib/JSON_file_ROIs/irida_config.json";
const std::string vis_config = "/home/sofoklis/Pictures/NewPeopleTrLib/JSON_file_ROIs/visualizationConfig.txt";


///////////////////////////// Creation of Tracker /////////////////////////////
std::unique_ptr<PedestrianTracker>
CreateTracker() {

    TrackerParams trackerPar(vis_config);

    std::unique_ptr<PedestrianTracker> tracker(new PedestrianTracker(trackerPar));

    return tracker;
};
///////////////////////////// End Tracker /////////////////////////////


int main(int argc, char *argv[]) {
    std::cout << "InferenceEngine: " << GetInferenceEngineVersion() << std::endl;
    // Reading command line parameters./home/sofoklis/Pictures/NewPeopleTrLib/Library/videos/Accenture_setup1_190227.mp4
    auto video_path ="/home/sofoklis/Pictures/NewPeopleTrLib/videos/Accenture_setup1_190227.mp4";//Bata_setup1_190227.mp4//Video6.avi//bata300.avi

    ///////////////////////////// Read ROIs /////////////////////////////
    //Read json files and save output text files
    /*std::vector<std::string> Keys ={"footfall", "dwelltime", "entrance"};

    ILsystem ILS(json_Path, Keys, false);

    vector<vector<vector<int>>> footfallREG_v = ILS.get_polygon("footfall");
    vector<vector<vector<int>>> dwellREG_v = ILS.get_polygon("dwelltime");
    vector<vector<vector<int>>> entranceLIN_v = ILS.get_polygon("entrance");
*/
    ///////////////////////////// END Read ROIs /////////////////////////////

    ///////////////////////////// Detectore /////////////////////////////

    ObjectDetector pedestrian_detector;

    /////////////////////////////////////////////////////////////////////

    ///////////////////////////// Tracker /////////////////////////////
    /*std::unique_ptr<PedestrianTracker> tracker =  CreateTracker();

    vector<vector<int>> footfall_id;
    vector<Point3i> people;
    if (footfallREG_v.size() > 0){
        for (size_t i=0; i<footfallREG_v.size(); i++){
            vector<int> tmp;
            footfall_id.push_back(tmp);
        }
    }

    tracker->ReadREGIONS(footfallREG_v, dwellREG_v, entranceLIN_v);*/
    ///////////////////////////// END Tracker /////////////////////////////

    ///////////////////////////// RGB video /////////////////////////////*/
    // Opening video.
    std::unique_ptr<ImageReader> video =
            ImageReader::CreateImageReaderForPath(video_path);


    typedef std::chrono::duration<double, std::ratio<1, 1000>> ms;
    bool debug = true;
    int start = 0;
    while (true){

        // First thread: Extract video frame.
        auto total_t0 = std::chrono::high_resolution_clock::now();
        auto pair = video->Read();
        cv::Mat frame = pair.first;
        int frame_idx = pair.second;



        if (frame.empty()) break;

        if (frame_idx >=start){
                    // Second thread: Make the detections video frame.
                    auto ped_time = std::chrono::high_resolution_clock::now();
                    pedestrian_detector.submitFrame(frame, frame_idx);
                    pedestrian_detector.waitAndFetchResults();

                    std::vector<Detection> detections = pedestrian_detector.getResults();
                   // pedestrian_detector.printResults(frame,frame_idx);
                   // pedestrian_detector.SaveResults(frame_idx);


                    // Third thread: Process the tracker.

                    cout<< "Frameidx  : " << frame_idx<<endl;

                    auto t0 = std::chrono::high_resolution_clock::now();
                    //tracker->Process(frame, detections, frame_idx, footfall_id);
                    //auto t1 = std::chrono::high_resolution_clock::now();

                    //Mat image = tracker->Visualize(frame_idx, frame);
                    auto t2 = std::chrono::high_resolution_clock::now();

                    //--------------- Execution Stats -----------------------//
                    if (debug) {
                        ms det_stats = std::chrono::duration_cast<ms>(t0 - ped_time);
                        //ms tracker_stats = std::chrono::duration_cast<ms>(t1 - t0);
                        //ms render_stats = std::chrono::duration_cast<ms>(t2 - t1);
                        ms total_stats = std::chrono::duration_cast<ms>(t2 - total_t0);

                        std::ostringstream out3;
                        out3 << "Detector + REID time  : " << std::fixed << std::setprecision(2) << det_stats.count()
                             << " ms ("
                             << 1000.f / det_stats.count() << " fps)";
                        cv::putText(frame, out3.str(), cv::Point2f(0, frame.rows - 80), cv::FONT_HERSHEY_TRIPLEX, 0.5,
                                    cv::Scalar(0, 0, 0));

                        /*std::ostringstream out;
                        out << "Tracker time  : " << std::fixed << std::setprecision(2) << tracker_stats.count()
                            << " ms ("
                            << 1000.f / tracker_stats.count() << " fps)";
                        cv::putText(image, out.str(), cv::Point2f(0, frame.rows - 60), cv::FONT_HERSHEY_TRIPLEX, 0.5,
                                    cv::Scalar(255, 0, 0));*/

                        /*std::ostringstream out1;
                        out1 << "Rendering time  : " << std::fixed << std::setprecision(2) << render_stats.count()
                             << " ms ("
                             << 1000.f / render_stats.count() << " fps)";
                        cv::putText(image, out1.str(), cv::Point2f(0, frame.rows - 40), cv::FONT_HERSHEY_TRIPLEX, 0.5,
                                    cv::Scalar(0, 255, 0));*/

                        std::ostringstream out2;
                        out2 << "Total time per frame  : " << std::fixed << std::setprecision(2) << total_stats.count()
                             << " ms ("
                             << 1000.f / total_stats.count() << " fps)";
                        cv::putText(frame, out2.str(), cv::Point2f(0, frame.rows - 20), cv::FONT_HERSHEY_TRIPLEX, 0.5,
                                    cv::Scalar(0, 0, 255));

                        std::ostringstream out5;
                        out5 << "Frame idx  : " << frame_idx;
                        cv::putText(frame, out5.str(), cv::Point2f(0, frame.rows - 100), cv::FONT_HERSHEY_TRIPLEX, 0.5,
                                    cv::Scalar(0, 255, 0));
                        // ----------------------------------------------------//
                    }

                    // Fourth thread: show the video frame.
                    cv::imshow("tracking", frame);
                    char k = cv::waitKey(1);
                    if (k == 27)
                    break;

                    // Audit operation //
                   /* if (AUDIT_MODE) {
                        string name = "/home/sofoklis/Pictures/NewPeopleTrLib/DeepSort/Output_Frames2/frame_" +
                                      to_string(frame_idx) +
                                      ".jpg";
                        imwrite(name, image);
                    }*/
            }
        // Output
        //ILS.save_tracks(people, frame);
    };

    cv::destroyAllWindows();

    /*if (true) {
        pedestrian_detector.PrintPerformanceCounts();
        pedestrian_detector.PrintPerformanceCounts_REID();
    }
*/
    return 0;
};