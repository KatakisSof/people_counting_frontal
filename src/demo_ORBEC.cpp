// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

/**
* \brief The entry point for the Inference Engine interactive_Vehicle_detection demo application
* \file security_barrier_camera_demo/main.cpp
* \example security_barrier_camera_demo/main.cpp
*/
#include "ILTracker.hpp"
#include "ILDetector.hpp"
#include "ILsystem.hpp"
//#include "image_reader.hpp"

#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <samples/ocv_common.hpp>
#include <iostream>
#include <utility>
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <gflags/gflags.h>

#include <fstream>
#include <ctype.h>

// OpenNI
#include <OpenNI.h>
#include "OniSampleUtilities.h"

using namespace std;
using namespace openni;
using namespace InferenceEngine;

// Video and frames parameters //
#define SAMPLE_READ_WAIT_TIMEOUT 10000 //orbbec
const int ROWS = 480;           // The number of rows (image) orbbec
const int COLS = 640;           // The number of cols (image) orbbec

const std::string json_Path = "/home/sofoklis/Pictures/NewPeopleTrLib/JSON_file_ROIs/irida_config.json";
const std::string vis_config = "/home/sofoklis/Pictures/NewPeopleTrLib/JSON_file_ROIs/visualizationConfig.txt";

auto FLAGS_devices = "MYRIAD";

const std::string &reid_model = "../models/squeezeREID/REID_res18_layer2.xml";
const std::string &reid_weights = fileNameNoExt(reid_model) + ".bin";

const std::string &attr_model = "../models/pedestrian/person-attributes-recognition-crossroad-0200/dldt/person-attributes-recognition-crossroad-0200-fp16.xml";
const std::string &attr_weights = fileNameNoExt(attr_model) + ".bin";

///////////////////////////// Tracker /////////////////////////////
std::unique_ptr<PedestrianTracker>
CreateTracker(const InferenceEngine::InferencePlugin& tracker_plugin) {

        TrackerParams trackerPar(vis_config);

        std::unique_ptr<PedestrianTracker> tracker(new PedestrianTracker(trackerPar));

        if (trackerPar.TOP_VIEW) {
            return tracker;
        }

        if (!reid_model.empty() && !reid_weights.empty()) {
            /*  CnnConfig reid_config(reid_model, reid_weights);*/
            std::shared_ptr<REIDvector> descriptor_strong =
                    std::make_shared<REIDvector>(reid_model, reid_weights, tracker_plugin);

            tracker->set_descriptor_strong(descriptor_strong);

        } else {
            std::cout << "WARNING: Either reid model or reid weights "
                      << "were not specified. " << std::endl;
        }

        if (!attr_model.empty() && !attr_weights.empty()) {
            /*  CnnConfig reid_config(reid_model, reid_weights);*/

            std::shared_ptr<AttrVector> descriptor_attr =
                    std::make_shared<AttrVector>(attr_model, attr_weights, tracker_plugin);

            tracker->set_descriptor_attr(descriptor_attr);

        } else {
            std::cout << "WARNING: Either attributes model or attributes weights "
                      << "were not specified. " << std::endl;
        }

    return tracker;
};
///////////////////////////// End Tracker /////////////////////////////


int main(int argc, char *argv[]) {
    std::cout << "InferenceEngine: " << GetInferenceEngineVersion() << std::endl;
    // Reading command line parameters.

    ///////////////////////////// Read ROIs /////////////////////////////
    //Read json files and save output text files
    std::vector<std::string> Keys ={"footfall", "dwelltime", "entrance"};

    ILsystem ILS(json_Path, Keys, false);

    vector<vector<vector<int>>> footfallREG_v = ILS.get_polygon("footfall");
    vector<vector<vector<int>>> dwellREG_v = ILS.get_polygon("dwelltime");
    vector<vector<vector<int>>> entranceLIN_v = ILS.get_polygon("entrance");

    ///////////////////////////// END Read ROIs /////////////////////////////

    ///////////////////////////// Detectore /////////////////////////////
    auto det_model = "../models/head_detector/head_det.xml";
    auto det_weights = fileNameNoExt(det_model) + ".bin";

    std::vector<std::string> devices{FLAGS_devices, FLAGS_devices, FLAGS_devices};

    std::map<std::string, InferencePlugin> plugins_for_devices =
        LoadPluginForDevices(devices, "", "", false);

    DetectorConfig detector_confid(det_model, det_weights);
    auto detector_plugin = plugins_for_devices[FLAGS_devices];
    ObjectDetector pedestrian_detector(detector_confid, detector_plugin);
    /////////////////////////////////////////////////////////////////////

    ///////////////////////////// Tracker /////////////////////////////
    auto tracker_plugin = plugins_for_devices[FLAGS_devices];
    std::unique_ptr<PedestrianTracker> tracker =  CreateTracker(tracker_plugin);

    vector<vector<int>> footfall_id;
    vector<Point3i> people;
    if (footfallREG_v.size() > 0){
        for (size_t i=0; i<footfallREG_v.size(); i++){
            vector<int> tmp;
            footfall_id.push_back(tmp);

        }
    }
    tracker->ReadREGIONS(footfallREG_v, dwellREG_v, entranceLIN_v);
    ///////////////////////////// END Tracker /////////////////////////////

    ///////////////////////////// Orbbec /////////////////////////////
    // Initiliazation//
    Status rc = OpenNI::initialize();
    std::cout << "After initialization" << std::endl;

    // Open device //
    Device device;
    rc = device.open(ANY_DEVICE);
    if (rc != STATUS_OK)
    {
        std::cout << "SimpleViewer: Device open failed" << std::endl;
        return 1;
    }

    // Depth videostream //
    VideoStream color, depth;
    rc = color.create(device, SENSOR_COLOR);
    if (rc != STATUS_OK)
    {
        std::cout << "SimpleViewer: Couldn't find color stream" << std::endl;
        return 1;
    }

    rc = depth.create(device, SENSOR_DEPTH);
    if (rc != STATUS_OK)
    {
        std::cout << "SimpleViewer: Couldn't find depth stream" << std::endl;
        return 1;
    }

    // Start streaming //
    std::cout << "Start Streaming" << std::endl;
    rc = color.start();
    if (rc != STATUS_OK)
    {
        std::cout << "SimpleViewer: Couldn't strat color stream" << std::endl;
        return 1;
    }

    // The current frame of Orbbec
    VideoFrameRef framecolor;
    ///////////////////////////// END Orbbec /////////////////////////////

    int frame_idx = 1;
    while (true){
        // Wait for frame and corvert orbbec frame to RGB image//
        int changedStreamDummy;
        VideoStream* pStreamColor = &color;
        rc = OpenNI::waitForAnyStream(&pStreamColor, 1, &changedStreamDummy, SAMPLE_READ_WAIT_TIMEOUT);
        rc = color.readFrame(&framecolor);
        cv::Mat rgb(ROWS, COLS, CV_8UC3, (void*)framecolor.getData());
        cv::Mat imo = cv::Mat::zeros(ROWS, COLS, CV_8UC3);
        cv::cvtColor(rgb, imo,    cv::COLOR_RGB2BGR);
        imo.convertTo(imo, CV_8UC3);
        cv::Mat frame = imo.clone();

        pedestrian_detector.submitFrame(frame, frame_idx);
        pedestrian_detector.waitAndFetchResults();
        TrackedObjects detections = pedestrian_detector.getResults();

        tracker->Process(frame, detections, frame_idx, footfall_id);
        Mat image = tracker->Visualize(frame_idx, frame);

        cv::imshow("tracking", image);
        char k = cv::waitKey(1);
        if (k == 27)
            break;

        // Output
        //ILS.save_tracks(people, frame);
        frame_idx++;
    };

    color.stop();
    color.destroy();
    device.close();
    OpenNI::shutdown();
    cv::destroyAllWindows();
    return 0;
};